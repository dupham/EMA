

#filepath="courbes/fils.bin"
#-------------------------------------------------------------------------------------------------------
# Fonction qui lit un fichier keysight
# BIN Headrer Format
# author : Hélène Le Bouder
#------------------------------------------------------------------------------------------------------------------------

function load_bin_keysight(filepath)
	# ouverture du fichier
	f = open(filepath, "r")

	if(eof(f) == true)
		close(f)
	return 0
	end

	# Cookie : 2 bytes characters, AG, which indicates that the file is in the Keysight Binary Data file format.
	# on doit retrouver AG soit 0x41 0x47
	Cookie=Array{UInt8}(undef, 2)
	read!(f, Cookie)

	if(Cookie!=[0x41,0x47])
        	println("Ce n'est pas du format BIN")
		close(f)
		return 0
	end

	# Version: 2 bytes which represent the file version.
	Version=Array{UInt8}(undef, 2)
	read!(f, Version)

	# FileSize: An integer (4 bytes signed) which is the number of bytes that are in the file.
    FileSize=read(f, Int32)

	#Number of Waveforms: An integer (4 bytse signed) which is the number of waveforms that are stored in the file.
	Number_Waveforms=read(f, Int32)
	M = []
	T = []

	for i=1:Number_Waveforms
		timetag=read_Waveform_Header(f)
		C=read_Waveform_Data_Header(f)
		push!(M, C)
		push!(T,timetag)
	end

	close(f)


	return (M, T)

end

#-------------------------------------------------------------------------------------------------------
# Read a read_Waveform_Header
# f file
# BIN Headrer Format
# author : Hélène Le Bouder
#------------------------------------------------------------------------------------------------------------------------
function read_Waveform_Header(f)

	#-----------------#
	# Waveform Header #
	#-----------------#

	#Header Size: An integer (4 bytes signed) which is the number of bytes in the header
	Header_Size=read(f, Int32)

	#Waveform Type: An integer (4 bytes signed) which is the type of waveform that is stored in the file.
	#The follow shows what each value means.
	#0 = Unknown
	#1 = Normal
	#2 = Peak Detect
	#3 = Average
	#4 = Horizontal Histogram
	#5 = Vertical Histogram
	#6 = Logic
	Waveform_Type=read(f, Int32)

	println("Waveform Type: ")
	if Waveform_Type == 0
        	println("Unknown \n")
		println(" error \n")
		close(f)
		return 0
    	elseif  Waveform_Type == 1
		println("Normal\n")
	elseif  Waveform_Type == 2
		println("Peak Detect\n")
	elseif  Waveform_Type == 3
		println("Average\n")
	elseif Waveform_Type == 4
		println("Horizontal Histogram\n")
	elseif  Waveform_Type == 5
		println("Vertical Histogram\n")
	elseif  Waveform_Type == 6
		println("Logic\n")
	else
		println(" error \n")
		close(f)
		return 0
	end

	#Number of Waveform Buffers: An integer (4 bytes signed) which is the number of waveform buffers required toread the data.
	#This value is one except for peak detect data and digital data.
	Number_of_Waveform_Buffers=read(f, Int32)

	Points=read(f, Int32)

	#Count: An integer (4 bytes signed)
	#which is the number of hits at each time bucket in the waveform record,
        #when the waveform was created using an acquisition mode likeaveraging.
	#For example, when averaging, a count of four would mean every waveform data point in the waveform record has been averaged at least four times.
	#The default value is 0.
	Count=read(f, Int32)

	#X Display Range: A float (8 bytes) which is the X-axis duration of the waveform that is displayed.
	#For time domain waveforms, it is the duration of time across the display.
	#If the value is zero then no data has been acquired.
	X_Display_Range=read(f, Float32)

	#X Display Origin: A double (8 bytes) which is the X-axis value at the left edge of the display.
	#For time domain waveforms, it is the time at the start of the display.
	#This value is treated as a double precision 64-bit floating point number.
	#If the value is zero then no data has been acquired.
	X_Display_Origin=read(f, Float64)

	#X Increment: A double (8 bytes) which is the duration between data points on the X axis.
	#For time domain waveforms, this is the time between points.
	#If the value is zero then no data has been acquired.
	X_Increment=read(f, Float64)

	#X Origin A double (8 bytes) which is the X-axis value of the first data point in the data record.
	#For time domain waveforms, it is the time of the first point.
	#This value is treated as a double precision 64-bit floating point number.
	#If the value is zero then no data has been acquired.
	X_Origin=read(f, Float64)

	#X Units An integer (4 bytes signed) which is the number of X units columns (n) depends on the number of sources being stored.
	#The X units is the unit of measure for each time value of the acquired data. X unit definitions are:
	#0 = Unknown
	#1 = Volt
	#2 = Second
	#3 = Constant
	#4 = Amp
	#5 = Decibel
	X_Units=read(f, Int32)

	println("Units X: ")
	if X_Units == 0
        	println("Unknown \n")
    	elseif  X_Units == 1
		println(" Volt\n")
	elseif  X_Units == 2
		println(" Second \n")
	elseif  X_Units == 3
		println(" Constant\n")
	elseif  X_Units == 4
		println(" Amp \n")
	elseif  X_Units == 5
		println(" Decibel \n")
	else
		println("error \n")
	end


	#Y Units: An integer (4 bytes signed) which is the number of Y units columns (n) depends on the number of sources being stored.
	#The Y units is the unit of measure of each voltage value of the acquired waveform.
	#Y units definitions are:
	#0 = Unknown
	#1 = Volt
	#2 = Second
	#3 = Constant
	#4 = Amp
	#5 = Decibel
	Y_Units=read(f, Int32)

	println("Units Y:")
	if X_Units == 0
        	println("Unknown \n")
    	elseif  Y_Units == 1
		println(" Volt\n")
	elseif  Y_Units == 2
		println(" Second \n")
	elseif  Y_Units == 3
		println(" Constant\n")
	elseif  Y_Units == 4
		println(" Amp \n")
	elseif  Y_Units == 5
		println(" Decibel \n")
	else
		println("\n error \n")

	end

	#Date: A 16 character array which is the date when the waveform was acquired.
	#The default value is 27 DEC 1996.
	Date=Array{UInt8}(undef,16)
	read!(f,Date)
	date=String(Date)
	println("Date :")
	println(date)
	println("\n")

	#Time: A 16 character array which is the time when the waveform was acquired.
	#The default value is 01:00:00:00.
	Time=Array{UInt8}(undef,16)
	read!(f,Time)

	time=String(Time)
	println("Time :")
	println(time)
	println("\n")

	#Frame: A 24 character array which is the model number and serial number of the scope in the format of MODEL#:SERIAL#.
	Frame=Array{UInt8}(undef,24)
	read!(f,Frame)

	frame=String(Frame)
	println("Frame :")
	println(frame)
	println("\n")

	#Waveform Label: A 16 character array which is the waveform label.
	Waveform_Label=Array{UInt8}(undef,16)
	read!(f,Waveform_Label)

	tmp=String(Waveform_Label)
	println("Waveform Label :")
	println(tmp)
	println("\n")

	#Time Tags: A double (8 bytes) which is the time tag value of the segment being saved.
	Time_Tags=read(f, Float64)

	#Segment Index: An unsigned integer (4 byte signed) which is the segment index of the data that follows the waveform data header.
	Segment_Index=read(f, UInt32)

	return Time_Tags
end





#-------------------------------------------------------------------------------------------------------
# Read a read_Waveform
# BIN Headrer Format
# author : Hélène Le Bouder
# Attention il peut y avoir des erreurs car la doc de keysight n'est pas précise
#------------------------------------------------------------------------------------------------------------------------
function read_Waveform_Data_Header(f)

	#----------------------#
	# Waveform Data Header #
	#----------------------#

	#Waveform Data Header Size: An integer (4 byte signed) which is the size of the waveform data header.
	Waveform_Data_Header_Size=read(f, Int32)

	#Buffer Type: A short (2 byte signed) which is the type of waveform data that is stored in the file.
	#The following shows what each value means.
	#0 = Unknown data
	#1 = Normal 32 bit float data
	#2 = Maximum float data
	#3 = Minimum float data
	#4 = Time float data
	#5 = Counts 32 bit float data
	#6 = Digital unsigned 8 bit char data
	Buffer_Type=read(f, Int16)


	#Bytes Per Point: A short (2 byte signed) which is the number of bytes per data point.
	Bytes_Per_Point=read(f, Int16)

	#Buffer Size :An integer (4 byte signed) which is the size of the buffer required to hold the data bytes.
	Buffer_Size=read(f, Int32)

	#Buffer Type:
	#0 = Unknown data
	#1 = Normal 32 bit float data
	#2 = Maximum float data
	#3 = Minimum float data
	#4 = Time float data
	#5 = Counts 32 bit float data
	#6 = Digital unsigned 8 bit char data

	println("\n Buffer_Type: ")
	if Buffer_Type == 0
		println("Unknown \n")
		close(f)
		return 0
    	elseif  Buffer_Type == 1
		println("Normal 32 bit float data\n")
		type_data= Float32
		Number_points= Int32(Buffer_Size/4)
	elseif  Buffer_Type == 2
		println("Maximum float data\n")
		type_data= Float64 # à vérifier
		Number_points= Int32(Buffer_Size/8)
	elseif  Buffer_Type == 3
		println("Minimum float data\n")
		type_data= UInt8 # à vérifier
		Number_points= Int32(Buffer_Size)
	elseif Buffer_Type == 4
		println("Time float data\n")
		type_data= Float32 # à vérifier
		Number_points= Int32(Buffer_Size/4)
	elseif  Buffer_Type == 5
		println("Counts 32 bit float data\n")
		type_data=Int32
		Number_points= Int32(Buffer_Size/4)
	elseif  Buffer_Type == 6
		println("Digital unsigned 8 bits char data\n")
		type_data=Uint8
		Number_points=Int32(Buffer_Size)
	else
		println("\n error Waveform Type impossible \n")
		close(f)
		return 0
	end

	print("\n Number points: ")
	println(Number_points)

	Curve = Array{type_data}(undef,Number_points)
	read!(f,Curve)
	# Curve=read(f, type_data, Number_points)

	return Curve
end
