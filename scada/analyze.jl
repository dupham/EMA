include("parseur.jl")
using Printf

dest_path = "/home/ronan/Documents/scada/22032018/"
dest_path_fil = "/home/ronan/Documents/scada/22032018/fil/"

MC=[]
TC=[]
MF=[]
TF=[]

for i=1:2

	(m,t)=load_bin_keysight("$(dest_path)c$(i).bin")
	push!(MC,m)
	push!(TC,t)
end


for i=1:2

	(m,t)=load_bin_keysight("$(dest_path_fil)f$(@sprintf("%02i", i)).bin", )
	push!(MF,m)
	push!(TF,t)
end

using Plots
# pyplot()

plot(MC[2][1])
plot!(MC[1][2])



# function plot_spectrogram(s,fs)
#     S = spectrogram(s[:,1], convert(Int, 25e-6),
#                     convert(Int, 10e-6); window=hanning)
#     t = time(S)
#     f = freq(S)
#     imshow(flipud(log10(power(S))), extent=[first(t), last(t),
#             first(f), last(f)], aspect="auto")
#     S
# end

using DSP
function plot_spectrogram(s, fs)
	S = spectrogram(s, round(Int, 25e-6*fs), round(Int, 10e-6*fs); window=hanning)
    t = time(S)
    f = freq(S)
    imshow(flipdim(log10.(power(S)),1), extent=[first(t), last(t),
            fs*first(f), fs*last(f)], aspect="auto")
end

using StatsBase

function peaks(m, th)
	indices=filter(x-> abs(m[x])>th, 1:size(m,1))

	map(x->(x,m[x]), indices)
end

# cc=crosscor(M[i][2], M[2][1], collect(1:100000))