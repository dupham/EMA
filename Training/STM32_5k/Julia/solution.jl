# @Author: Hélène Le Bouder, Ronan Lashermes
# @Date:   19-10-2016
# @Email:  helene.le-bouder@inria.fr, ronan.lashermes@inria.fr
# @License: CC-by-nc

using Statistics
using CSV
using SharedArrays

include("loaders.jl")
include("AES.jl")

#load traces and ciphertexts
data = load_traces("../data.bin")
texts = load_texts("../texts.bin")

# Prediction for given cipher byte and key byte
function gen_prediction(cipher_byte, key_byte)
  return count_ones(invsbox(xor(cipher_byte, key_byte)))#count_ones is Hamming Weight
end

# Generate prediction vectors for all key guesses when targeting a specific byte of the key
function generate_prediction_matrix(ciphers, targeted_byte)
 n = size(ciphers, 1)
 byte_count = size(texts, 2)
 pred_M = round.(UInt8, zeros(n, 256))
 for k=0:255#for each key guess on one byte
   pred_M[:,k+1] = map(c -> gen_prediction(c,k), ciphers[:,targeted_byte])
 end
 return pred_M
end

#Find one key byte
function find_key_byte(data, texts, targeted_byte)
  prediction = generate_prediction_matrix(texts, targeted_byte)
  prediction = convert.(Float64,prediction)
  n = size(data, 1)# number of traces
  p = size(data, 2)# Number of points in a trace
  # cor_res = zeros(p, 256)# compute a correlation for each time and each key guess
  cor_res = SharedArray{Float64, 2}(p, 256)
  dataF = convert.(Float64,data)

  Threads.@threads for k=1:256#for each key guess
    for t=1:p#for each time
      cor_res[t,k] = cor(dataF[:,t], prediction[:,k])
    end
    # println("Byte $targeted_byte, key guess $(k-1) evaluated.")
  end
  cor_res
end

#Find the whole key
function find_key(data, texts)
  klen = size(texts, 2)
  K10 = round.(UInt8, zeros(klen))
  for b=1:klen
    r = find_key_byte(data,texts,b)
    maxvalind = findmax(abs.(r))[2]
    K10[b] = maxvalind[2]-1 #-1 to account for index starts at 1 (key value starts at 0)
    println("Key value for byte $(b-1) is $(K10[b])!")
  end
  K10
end

function get_correct_K10()
  master_key_df = CSV.read("keyCorrect.csv", header=0, allowmissing=:none)
  master_key = convert(Array,master_key_df)
  reshape(KeyGen(master_key)[11,:,:],1,16)
end
