# @Author: Hélène Le Bouder, Ronan Lashermes
# @Date:   19-10-2016
# @Email:  helene.le-bouder@inria.fr, ronan.lashermes@inria.fr
# @License: CC-by-nc

function load_traces(filepath::String)
  f = open(filepath, "r")

  if(eof(f) == true)# if file empty, quit
    close(f)
    return 0
  end

  trace_len::Int32 = read(f, Int32)# read trace length
  trace_count::Int32 = read(f, Int32)# read trace count

  # println("Trace len $(trace_len), trace count $(trace_count )")

  M=zeros(Int8, trace_count, trace_len)#init the result matrix

  i::Int32 = 1
  temp = Array{Int8}(undef,trace_len)

  while eof(f) == false && i <= trace_count # read each trace and put in matrix
    # M[i, :] = read(f, Int8, trace_len)
    read!(f, temp)
    M[i,:] = temp
    i+=1
  end

  close(f)
  return M

end


function load_texts(filepath::String)
  f_texts = open(filepath, "r")
	texts=map(x->hex2bytes(strip(x)),readlines(f_texts))#one liner to read all texts
	close(f_texts)

  # convert array of array to matrix
  n=size(texts,1)
	p=size(texts[1],1)
	mt=zeros(n,p)
	for i in 1:n
			mt[i,:]=texts[i]
	end
	round.(UInt8,mt)

end
