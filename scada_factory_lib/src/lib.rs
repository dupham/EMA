/*
 * File: lib.rs
 * Project: src
 * Created Date: Tuesday August 21st 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 28th February 2019 11:06:23 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

// extern crate rand;
// extern crate nalgebra;
// extern crate toml;
// #[macro_use] extern crate failure;
// extern crate rand_pcg;

pub mod signals;
pub mod factory;
pub mod trace;