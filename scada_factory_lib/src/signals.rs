/*
 * File: signal.rs
 * Project: factory
 * Created Date: Thursday February 28th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 28th February 2019 11:02:48 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

pub mod apparatus;
pub mod peak;

use self::peak::Peak;

pub const TIMING_RESOLUTION: f64 = 0.000001f64;
pub const INV_TIMING_RESOLUTION: f64 = 1f64 / TIMING_RESOLUTION;