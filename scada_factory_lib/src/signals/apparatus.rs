/*
 * File: apparatus.rs
 * Project: src
 * Created Date: Tuesday August 21st 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 28th February 2019 11:58:57 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use rand::prelude::*;

use std::collections::BTreeMap;

use crate::signals::Peak;
use crate::trace::Trace;

#[derive(Debug,Clone)]
pub struct Apparatus {
    signature: Vec<Peak>
}

impl Apparatus {
    pub fn new_randomized<R: Rng, 
                            JD: Distribution<f64>, 
                            VD: Distribution<f64>, 
                            SD: Distribution<f64>>
        (cycle_duration: f64, peak_count: u64, jitter_distrib: &JD, value_distrib: &VD, std_distrib: &SD, rng: &mut R) -> Apparatus {

        let mut peaks = Vec::new();

        for _ in 0..peak_count {
            let time = rng.gen_range(0f64, cycle_duration);
            let jitter = rng.sample(jitter_distrib);

            let value = rng.sample(value_distrib);
            let stddev = rng.sample(std_distrib);

            peaks.push(Peak::new(time, jitter, value, stddev));
        }

        Apparatus { signature: peaks }
    }

    pub fn act<R: Rng>(&self, start_time: i64, rng: &mut R) -> Trace {
        let mut result: BTreeMap<i64, f64> = BTreeMap::new();
        for peak in self.signature.iter() {
            let (peak_time, peak_val) = peak.evaluate_at(rng);

            result.insert(peak_time + start_time, peak_val);
        }

        Trace::from_tree_map(result)
    }
}

#[test]
fn test_apparatus() {
    use rand::distributions::Normal;
    use rand::distributions::Uniform;

    let mut rng = thread_rng();
    let jitterdistrib = Normal::new(0f64, 1f64);
    let stddistrib = Normal::new(0f64, 0.5f64);
    let vdistrib = Uniform::new(4f64, 8f64);

    let apparatus = Apparatus::new_randomized(10f64, 23, &jitterdistrib, &vdistrib, &stddistrib, &mut rng);

    let _res = apparatus.act(2, &mut rng);

}