/*
 * File: peak.rs
 * Project: src
 * Created Date: Tuesday August 21st 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 28th February 2019 11:38:59 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use rand::Rng;
use rand::prelude::*;
use rand::distributions::Normal;

use super::INV_TIMING_RESOLUTION;

#[derive(Debug,Clone)]
pub struct Peak {
    peak_timing_distrib: Normal,
    peak_value_distrib: Normal
}

impl Peak {
    //vdistrib: distrib to choose peak value stddev, hdistrib: distrib to choose jitter
    pub fn random<R: Rng, HD: Distribution<f64>, VD: Distribution<f64>>(time: f64, mean: f64, vdistrib: VD, hdistrib: HD, rng: &mut R) -> Peak {
        let jitter = rng.sample(hdistrib).abs();
        let stddev = rng.sample(vdistrib).abs();

        let tdistrib = Normal::new(time, jitter);
        let vdistrib = Normal::new(mean, stddev);

        Peak { peak_timing_distrib: tdistrib, peak_value_distrib: vdistrib }
    }

    pub fn new(time: f64, jitter: f64, value: f64, stddev: f64) -> Peak {
        let tdistrib = Normal::new(time, jitter.abs());
        let vdistrib = Normal::new(value, stddev.abs());

        Peak { peak_timing_distrib: tdistrib, peak_value_distrib: vdistrib }
    }

    pub fn evaluate_at<R: Rng>(&self, rng: &mut R) -> (i64, f64) {
        let timing = rng.sample(self.peak_timing_distrib);
        let value = rng.sample(self.peak_value_distrib);

        let itiming = (timing * INV_TIMING_RESOLUTION) as i64;

        (itiming, value)
    }
}

#[test]
fn test_peaks() {
    let mut rng = thread_rng();
    let vdistrib = Normal::new(0f64, 0.5f64);
    let hdistrib = Normal::new(0f64, 5f64);

    let peak1 = Peak::random(5f64, 12f64, vdistrib, hdistrib, &mut rng);

    for _ in 0..10 {
        let _v = peak1.evaluate_at(&mut rng);
        println!("New peak 1 point: {:?}", _v);
    }
}