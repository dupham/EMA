/*
 * File: mod.rs
 * Project: product
 * Created Date: Monday December 10th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 10th December 2018 4:39:27 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

pub mod product_definition;
pub mod product_requirement;
pub mod product_process;

pub use self::product_definition::ProductDefinition;
pub use self::product_requirement::ProductRequirement;
pub use self::product_process::ProductProcess;