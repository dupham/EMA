/*
 * File: product_process.rs
 * Project: product
 * Created Date: Monday December 10th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 1st March 2019 11:02:16 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use toml::value::Array;
use failure::{Error, format_err};

#[derive(Debug,Clone)]
pub struct ProductProcess {
    pub machine_names: Vec<String>,
}

impl ProductProcess {
    pub fn new(process_arr: &Array) -> Result<ProductProcess, Error> {
        let mut machine_names = Vec::new();

        for mname_val in process_arr.iter() {
            let mname = mname_val.as_str().ok_or(format_err!("Machine name is not a string"))?;
            machine_names.push(mname.to_owned());
        }

        Ok(ProductProcess { machine_names })
    }
}