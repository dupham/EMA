/*
 * File: product_requirement.rs
 * Project: product
 * Created Date: Monday December 10th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 28th February 2019 11:04:28 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use toml::Value;
use failure::{Error, format_err};

#[derive(Debug,Clone)]
pub struct ProductRequirement {
    pub name: String,
    pub quantity: u64
}

impl ProductRequirement {
    pub fn new(val: &Value) -> Result<ProductRequirement, Error> {
        let name = val["name"].as_str().ok_or(format_err!("No name defined for this required product"))?;
        let quantity = val["quantity"].as_integer().ok_or(format_err!("No quantity defined for this required product"))?;

        Ok(ProductRequirement {
            name: name.to_owned(),
            quantity: quantity as u64
        })
    }
}