/*
 * File: product_definition.r
 * Project: factory
 * Created Date: Monday December 10th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 1st March 2019 11:02:36 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use toml::Value;
use failure::{Error, format_err};

use crate::factory::product::{ProductRequirement, ProductProcess};

#[derive(Debug,Clone)]
pub struct ProductDefinition {
    pub name: String,
    pub requires: Vec<ProductRequirement>, //list of subproduct requirements
    pub process: Option<ProductProcess>,
    pub production_count: u64
}

impl ProductDefinition {
    pub fn new(val: &Value) -> Result<ProductDefinition, Error> {
        let name = val["name"].as_str().ok_or(format_err!("No name defined for this product"))?;
        let mut requires = Vec::new();

        match val.get("requires") {
            Some(Value::Array(ref req_arr)) => {

                // println!("{} requires {:?}", name, req_arr);
                for req_val in req_arr.iter() {
                    let prod_req = ProductRequirement::new(req_val)?;
                    requires.push(prod_req);
                }
            },
            _ => {}
        }

        let production_count = match val.get("production_count") {
            Some(Value::Integer(i)) => *i as u64,
            _ => 1u64
        };
        
        let product_process = match val.get("process") {
            Some(Value::Array(ref process_arr)) => {
                Some(ProductProcess::new(process_arr)?)
            },
            _ => None
        };

        Ok(ProductDefinition {
            name: name.to_owned(),
            requires: requires,
            process: product_process,
            production_count
        })
    }
}