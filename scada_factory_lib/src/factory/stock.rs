/*
 * File: stock_rs
 * Project: factory
 * Created Date: Thursday December 13th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 1st March 2019 11:05:38 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use failure::{Error, format_err};

use std::collections::HashMap;

#[derive(Debug,Clone)]
pub struct Stock {
    products: HashMap<String, u64>
}

impl Stock {
    pub fn new() -> Stock {
        Stock { products: HashMap::new() }
    }

    pub fn add(&mut self, product_name: &str, product_quantity: u64) {
        let prev_count = match self.products.remove(product_name) {
            Some(i) => i,
            None => 0
        };

        self.products.insert(product_name.to_owned(), prev_count + product_quantity);
    }

    pub fn remove(&mut self, product_name: &str, product_quantity: u64) -> Result<(), Error> {
        let prev_count = match self.products.remove(product_name) {
            Some(i) => i,
            None => 0
        };

        if prev_count < product_quantity {
            return Err(format_err!("Cannot remove more products than what is in stock."));
        }

        self.products.insert(product_name.to_owned(), prev_count - product_quantity);
        Ok(())
    }

    pub fn get_stock_count(&self, product_name: &str) -> u64 {
        *self.products.get(product_name).unwrap_or(&0)
    }

    pub fn empty_stock(&mut self, product_name: &str) {
        self.products.remove(product_name);
    }
}