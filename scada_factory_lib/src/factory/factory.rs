/*
 * File: factory.rs
 * Project: src
 * Created Date: Monday December 10th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 1st March 2019 11:09:54 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use toml::Value;
use rand::Rng;
use failure::{Error, format_err};

use std::path::Path;
use std::fs::File;
use std::io::Read;
use std::collections::HashMap;

use crate::factory::{MachineDefinition, ProductDefinition, Stock};
use crate::trace::Trace;
use crate::signals::apparatus::Apparatus;
use crate::signals::INV_TIMING_RESOLUTION;

#[derive(Debug)]
pub struct Factory {
    rng: rand_pcg::Pcg32,
    machines: HashMap<String, MachineDefinition>,
    apparatus: HashMap<String, Apparatus>,
    products: HashMap<String, ProductDefinition>
}

impl Factory {

    /****************** PARSING  ***********************/
    /// Extract products from toml value
    fn extract_products(toml_products: &Value) -> Result<HashMap<String, ProductDefinition>, Error> {
        let mut products = HashMap::new();

        match toml_products {
            Value::Array(prod_arr) => {
                for toml_prod in prod_arr.iter() {
                    let product = ProductDefinition::new(toml_prod)?;
                    products.insert(product.name.clone(), product);
                }
            },
            _ => {}
        }

        Ok(products)
    }

    /// Extract machines from toml value
    fn extract_machines(toml_machines: &Value) -> Result<HashMap<String, MachineDefinition>, Error> {
        let mut machines = HashMap::new();

        match toml_machines {
            Value::Array(mach_arr) => {
                for toml_mach in mach_arr.iter() {
                    let machine = MachineDefinition::new(toml_mach)?;
                    machines.insert(machine.name.clone(), machine);
                }
            },
            _ => {}
        }

        Ok(machines)
    }

    pub fn parse_factory_config<P: AsRef<Path>>(path: P) -> Result<Factory, Error> {
        let mut file = File::open(path)?;
        let mut file_content = String::new(); 
        file.read_to_string(&mut file_content)?;
        let value = file_content.parse::<Value>()?;

        let seed = match value.get("seed") {
            Some(Value::Integer(i)) => Some(*i as u64),
            _ => None
        };
        let mut rng = Factory::new_rng(seed);

        let machines = Factory::extract_machines(&value["machines"])?;
        let products = Factory::extract_products(&value["products"])?;

        
        let apparatus = Factory::build_apparatus_from_machines_definitions(&machines, &mut rng);


        Ok(Factory { 
            rng,
            machines,
            apparatus,
            products
         })
    }

    fn build_apparatus_from_machines_definitions<R: Rng>(machines_def: &HashMap<String, MachineDefinition>, rng: &mut R) -> HashMap<String, Apparatus> {
        let mut res: HashMap<String, Apparatus> = HashMap::new();

        for (mname, mdef) in machines_def.iter() {
            let msignals = &mdef.signals;

            let app = Apparatus::new_randomized(msignals.cycle_duration, 
                                                msignals.peak_count, 
                                                &msignals.jitter_distribution,
                                                &msignals.peak_value_distribution,
                                                &msignals.peak_stddev_distribution,
                                                rng);

            res.insert(mname.to_owned(), app);
        }

        res
    }

    /*********** VARIOUS *******/
    pub fn new_rng(seed: Option<u64>) -> rand_pcg::Pcg32 {
        let rng = match seed {
            Some(s) => rand_pcg::Pcg32::new(s, 0),
            None => {rand_pcg::Pcg32::new(0, 0)}
        };

        rng
    }


    /********* RUNNING *******************/

    
    pub fn produce_many(&mut self, product_name: &str, count: u64) -> Result<(i64, Trace, Stock), Error> {
        let mut stock = Stock::new();
        let (duration, trace) = self.recursive_produce_many(&mut stock, product_name, count)?;
        Ok((duration, trace, stock))
    }

    /// This is the recursive scheduling algorithm: when and how to build products
    fn recursive_produce_many(&mut self, stock: &mut Stock, pname: &str, pcount: u64) -> Result<(i64, Trace), Error> {

        let mut trace: Trace = Trace::new();
        let mut run_duration: i64 = 0;

        //keep productiong till we have everything in stock
        while stock.get_stock_count(pname) < pcount {
            //do 1 production run

            //product to build definition
            let pdef = self.products.get(pname).ok_or(format_err!("Cannot find product {}.", pname))?.clone();

            //what the product to build requires
            let required_subproducts = pdef.requires.clone();

            //for each required subproduct
            for required_subproduct in required_subproducts.iter() {
                //first produce at least the needed amount of subproducts
                let subpname = &required_subproduct.name;
                let subpcount = required_subproduct.quantity;
                let (subrun_duration, subrun_trace) = self.recursive_produce_many(stock, subpname, subpcount)?;

                //update the trace and the running duration        
                trace += subrun_trace;

                //run_duration is max of subruns
                if subrun_duration > run_duration {
                    run_duration = subrun_duration;
                }

                //now we consume the needed amount of subproducts
                stock.remove(subpname, subpcount)?;
            }

            //now we can launch the process run for this product 
            match &pdef.process {
                Some(ref process) => {
                    //the process requires some machines running in parallel

                    //run duration of the slowest machine
                    let mut max_cycle_duration: i64 = 0;

                    //in the process, we have the names of the required machines
                    let machines_names = &process.machine_names;

                    //we iterate over these names
                    for machine_name in machines_names.iter() {
                        //get the apparatus from machine name
                        let app = self.apparatus.get(machine_name).ok_or(format_err!("Cannot find apparatus {}.", machine_name))?;
                        //get the machine def from machine name
                        let mdef = self.machines.get(machine_name).ok_or(format_err!("Cannot find machine {}.", machine_name))?;
                        // include duration in max calculation
                        let ic_duration: i64 = (mdef.signals.cycle_duration * INV_TIMING_RESOLUTION) as i64;
                        if max_cycle_duration < ic_duration {
                            max_cycle_duration = ic_duration;
                        }

                        //the apparatus generates a trace
                        let this_app_trace = app.act(run_duration, &mut self.rng);
                        trace += this_app_trace;
                    }

                    //run_duration is incremented with the duration of the slowest machine
                    run_duration += max_cycle_duration;
                },
                None => {}
            }

            //with this run we have produced [production_count] products
            let production_count = pdef.production_count;
            stock.add(pname, production_count);

        }
        

        Ok((run_duration, trace))
    }
    
}

#[test]
fn test_factory_parsing() {
    let factory = Factory::parse_factory_config("./factory_examples/test.toml");
    assert!(factory.is_ok(), "{:?}", factory);
    // println!("Factory is {:?}", factory);
}

#[test]
fn test_factory_bakery() {
    let factory = Factory::parse_factory_config("./factory_examples/bakery.toml");
    assert!(factory.is_ok(), "{:?}", factory);
    // println!("Factory is {:?}", factory);
}

#[test]
fn test_bakery() {
    let mut factory = Factory::parse_factory_config("./factory_examples/bakery.toml").unwrap();
    let _trace = factory.produce_many("bread", 10).unwrap();
    // println!("Bakery trace {:?}", _trace);
}

#[test]
fn test_simple() {
    let mut factory = Factory::parse_factory_config("./factory_examples/simple.toml").unwrap();
    let trace = factory.produce_many("2", 4).unwrap();
    println!("Simple trace {:?}", trace);
}