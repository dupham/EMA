/*
 * File: factory_node.rs
 * Project: src
 * Created Date: Monday December 10th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 10th December 2018 10:46:22 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

#[derive(Debug)]
pub struct FactoryNode {
    children: Vec<FactoryNode>,
}