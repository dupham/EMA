#include "picolib.h"


PICO_STATUS result;
UNIT unitPico;

unsigned long timebase = 0;//espacement entre les échantillons
long nbSamples = 10000; //nombre d'échantillons
short       oversample = 1;
bool      scaleVoltages = TRUE;

unsigned short inputRanges [PS3000A_MAX_RANGES] = {	10,
	20,
	50,
	100,
	200,
	500,
	1000,
	2000,
	5000,
	10000,
	20000,
	50000};

//open connection with picoscope
void openPico(void)
{
	int max_samples;

	result=ps3000aOpenUnit(&(unitPico.handle),NULL);
	if(result == PICO_POWER_SUPPLY_NOT_CONNECTED) //le picoscope demande que l'alimentation externe soit branchée
	{
		//on va lui dire que l'alimentation se fera via l'USB
		result = ps3000aChangePowerSource(unitPico.handle, PICO_POWER_SUPPLY_NOT_CONNECTED);
	}

	get_info(&unitPico);

	DisableAnalogue();//desactive tous les channel

	short value;
	ps3000aMaximumValue(unitPico.handle, &value);
	unitPico.maxValue = value;

	printf("Opening ********************** ");
	printError();
	printf("\n");

	/*result = ps3000aMemorySegments(unitPico.handle, 1, &max_samples);
	if(result == PICO_OK)
	{
		printf("Max samples: %d\n", max_samples);
	}
	*/
}

/****************************************************************************
* adc_to_mv
*
* Convert an 16-bit ADC count into millivolts
****************************************************************************/
int adc_to_mv(long raw, int ch)
{
	return (raw * inputRanges[ch]) / unitPico.maxValue;
}

/****************************************************************************
* mv_to_adc
*
* Convert a millivolt value into a 16-bit ADC count
*
*  (useful for setting trigger thresholds)
****************************************************************************/
short mv_to_adc(short mv, short ch)
{
	//printf("(MV: %i * Max: %i) / range %i.\n", mv, unitPico.maxValue, inputRanges[ch]);
	//short res = (mv * unitPico.maxValue) / inputRanges[ch];
	//printf("ADC: %i.\n", res);

	return (mv * unitPico.maxValue) / inputRanges[ch];
}


//close connection with picoscope
void closePico(void)
{
	result = ps3000aCloseUnit(unitPico.handle);
	printf("Closing ********************** ");
	printError();
	printf("\n");
}

//print error if any
void printError(void)
{
	switch(result)
	{
		case PICO_OK:
			printf("OK");
			break;
		case PICO_NOT_FOUND:
			printf("ERROR: Pico not found");
			break;
		case PICO_OPEN_OPERATION_IN_PROGRESS:
			printf("ERROR: PICO_OPEN_OPERATION_IN_PROGRESS");
			break;
		case PICO_OPERATION_FAILED:
			printf("ERROR: PICO_OPERATION_FAILED");
			break;
		case PICO_NOT_RESPONDING:
			printf("ERROR: PICO_NOT_RESPONDING");
			break;
		case PICO_INVALID_HANDLE:
			printf("ERROR: PICO_INVALID_HANDLE");
			break;
		case PICO_INVALID_PARAMETER:
			printf("ERROR: PICO_INVALID_PARAMETER");
			break;
		case PICO_INVALID_TIMEBASE:
			printf("ERROR: PICO_INVALID_TIMEBASE");
			break;
		case PICO_INVALID_TRIGGER_CHANNEL:
			printf("ERROR: PICO_INVALID_TRIGGER_CHANNEL");
			break;
		default:
			printf("ERROR: %i",result);
			break;
	}
}

//get infos from the picoscope model
void get_info(UNIT * unit)
{
	char description [6][25]= { "Driver Version",
		"USB Version",
		"Hardware Version",
		"Variant Info",
		"Serial",
		"Error Code" };
	short i, r = 0;
	char line [80];
	int variant;
	PICO_STATUS status = PICO_OK;

	if (unit->handle)
	{
		for (i = 0; i < 5; i++)
		{
			status = ps3000aGetUnitInfo(unit->handle, line, sizeof (line), &r, i);
			if (i == 3)
			{
				variant = atoi(line);
				//To identify varians.....

				if (strlen(line) == 5)					// A or B variant unit
				{
					line[4] = toupper(line[4]);

					if (line[1] == '2' && line[4] == 'A')		// i.e 3204A -> 0xA204
						variant += 0x9580;
					else
						if (line[1] == '2' && line[4] == 'B')		//i.e 3204B -> 0xB204
							variant +=0xA580;
						else
							if (line[1] == '4' && line[4] == 'A')		// i.e 3404A -> 0xA404
								variant += 0x96B8;
							else
								if (line[1] == '4' && line[4] == 'B')		//i.e 3404B -> 0xB404
									variant +=0xA6B8;
				}

				if (strlen(line) == 7)
				{
					line[4] = toupper(line[4]);
					line[5] = toupper(line[5]);
					line[6] = toupper(line[6]);

					if(strcmp(line+4, "MSO") == 0)
						variant += 0xc580;						// 3204MSO -> 0xD204
				}
			}
			printf("%s: %s\n", description[i], line);
		}

		switch (variant)
		{
		case MODEL_PS3204A:
			unit->model			= MODEL_PS3204A;
			unit->sigGen		= SIGGEN_FUNCTGEN;
			unit->firstRange	= PS3000A_50MV;
			unit->lastRange		= PS3000A_20V;
			unit->channelCount	= DUAL_SCOPE;
			unit->ETS			= FALSE;
			unit->AWGFileSize	= MIN_SIG_GEN_BUFFER_SIZE;
			unit->digitalPorts	= 0;
			break;

		case MODEL_PS3204B:
			unit->model			= MODEL_PS3204B;
			unit->sigGen		= SIGGEN_AWG;
			unit->firstRange	= PS3000A_50MV;
			unit->lastRange		= PS3000A_20V;
			unit->channelCount	= DUAL_SCOPE;
			unit->ETS			= FALSE;
			unit->AWGFileSize	= MAX_SIG_GEN_BUFFER_SIZE;
			unit->digitalPorts	= 0;
			break;

		case MODEL_PS3205A:
			unit->model			= MODEL_PS3205A;
			unit->sigGen		= SIGGEN_FUNCTGEN;
			unit->firstRange	= PS3000A_50MV;
			unit->lastRange		= PS3000A_20V;
			unit->channelCount	= DUAL_SCOPE;
			unit->ETS			= TRUE;
			unit->AWGFileSize	= MIN_SIG_GEN_BUFFER_SIZE;
			unit->digitalPorts	= 0;
			break;

		case MODEL_PS3205B:
			unit->model			= MODEL_PS3205B;
			unit->sigGen		= SIGGEN_AWG;
			unit->firstRange	= PS3000A_50MV;
			unit->lastRange		= PS3000A_20V;
			unit->channelCount	= DUAL_SCOPE;
			unit->ETS			= TRUE;
			unit->AWGFileSize	= MAX_SIG_GEN_BUFFER_SIZE;
			unit->digitalPorts	= 0;
			break;

		case MODEL_PS3206A:
			unit->model			= MODEL_PS3206A;
			unit->sigGen		= SIGGEN_FUNCTGEN;
			unit->firstRange	= PS3000A_50MV;
			unit->lastRange		= PS3000A_20V;
			unit->channelCount	= DUAL_SCOPE;
			unit->ETS			= TRUE;
			unit->AWGFileSize	= MIN_SIG_GEN_BUFFER_SIZE;
			unit->digitalPorts	= 0;
			break;

		case MODEL_PS3206B:
			unit->model			= MODEL_PS3206B;
			unit->sigGen		= SIGGEN_AWG;
			unit->firstRange	= PS3000A_50MV;
			unit->lastRange		= PS3000A_20V;
			unit->channelCount	= DUAL_SCOPE;
			unit->ETS			= TRUE;
			unit->AWGFileSize	= PS3206B_MAX_SIG_GEN_BUFFER_SIZE;
			unit->digitalPorts	= 0;
			break;

		case MODEL_PS3404A:
			unit->model			= MODEL_PS3404A;
			unit->sigGen		= SIGGEN_FUNCTGEN;
			unit->firstRange	= PS3000A_50MV;
			unit->lastRange		= PS3000A_20V;
			unit->channelCount	= QUAD_SCOPE;
			unit->ETS			= FALSE;
			unit->AWGFileSize	= MIN_SIG_GEN_BUFFER_SIZE;
			unit->digitalPorts	= 0;
			break;

		case MODEL_PS3404B:
			unit->model			= MODEL_PS3404B;
			unit->sigGen		= SIGGEN_AWG;
			unit->firstRange	= PS3000A_50MV;
			unit->lastRange		= PS3000A_20V;
			unit->channelCount	= QUAD_SCOPE;
			unit->ETS			= FALSE;
			unit->AWGFileSize	= MAX_SIG_GEN_BUFFER_SIZE;
			unit->digitalPorts	= 0;
			break;

		case MODEL_PS3405A:
			unit->model			= MODEL_PS3405A;
			unit->sigGen		= SIGGEN_FUNCTGEN;
			unit->firstRange	= PS3000A_50MV;
			unit->lastRange		= PS3000A_20V;
			unit->channelCount	= QUAD_SCOPE;
			unit->ETS			= TRUE;
			unit->AWGFileSize	= MIN_SIG_GEN_BUFFER_SIZE;
			unit->digitalPorts	= 0;
			break;

		case MODEL_PS3405B:
			unit->model			= MODEL_PS3405B;
			unit->sigGen		= SIGGEN_AWG;
			unit->firstRange	= PS3000A_50MV;
			unit->lastRange		= PS3000A_20V;
			unit->channelCount	= QUAD_SCOPE;
			unit->ETS			= TRUE;
			unit->AWGFileSize	= MAX_SIG_GEN_BUFFER_SIZE;
			unit->digitalPorts	= 0;
			break;

		case MODEL_PS3406A:
			unit->model			= MODEL_PS3406A;
			unit->sigGen		= SIGGEN_FUNCTGEN;
			unit->firstRange	= PS3000A_50MV;
			unit->lastRange		= PS3000A_20V;
			unit->channelCount	= QUAD_SCOPE;
			unit->ETS			= TRUE;
			unit->AWGFileSize	= MIN_SIG_GEN_BUFFER_SIZE;
			unit->digitalPorts	= 0;
			break;

		case MODEL_PS3406B:
			unit->model			= MODEL_PS3406B;
			unit->sigGen		= SIGGEN_AWG;
			unit->firstRange	= PS3000A_50MV;
			unit->lastRange		= PS3000A_20V;
			unit->channelCount	= QUAD_SCOPE;
			unit->ETS			= TRUE;
			unit->AWGFileSize	= PS3206B_MAX_SIG_GEN_BUFFER_SIZE;
			unit->digitalPorts	= 0;
			break;

		case MODEL_PS3204MSO:
			unit->model			= MODEL_PS3204MSO;
			unit->sigGen		= SIGGEN_AWG;
			unit->firstRange	= PS3000A_50MV;
			unit->lastRange		= PS3000A_20V;
			unit->channelCount	= DUAL_SCOPE;
			unit->ETS			= FALSE;
			unit->AWGFileSize	= MAX_SIG_GEN_BUFFER_SIZE;
			unit->digitalPorts	= 2;
			break;

		case MODEL_PS3205MSO:
			unit->model			= MODEL_PS3205MSO;
			unit->sigGen		= SIGGEN_AWG;
			unit->firstRange	= PS3000A_50MV;
			unit->lastRange		= PS3000A_20V;
			unit->channelCount	= DUAL_SCOPE;
			unit->ETS			= TRUE;
			unit->AWGFileSize	= MAX_SIG_GEN_BUFFER_SIZE;
			unit->digitalPorts	= 2;
			break;

		case MODEL_PS3206MSO:
			unit->model			= MODEL_PS3206MSO;
			unit->sigGen		= SIGGEN_AWG;
			unit->firstRange	= PS3000A_50MV;
			unit->lastRange		= PS3000A_20V;
			unit->channelCount	= DUAL_SCOPE;
			unit->ETS			= TRUE;
			unit->AWGFileSize	= PS3206B_MAX_SIG_GEN_BUFFER_SIZE;
			unit->digitalPorts	= 2;
			break;

		case MODEL_PS3207A:
			unit->model			= MODEL_PS3207A;
			unit->sigGen		= SIGGEN_FUNCTGEN;
			unit->firstRange	= PS3000A_50MV;
			unit->lastRange		= PS3000A_20V;
			unit->channelCount	= DUAL_SCOPE;
			unit->ETS			= TRUE;
			unit->AWGFileSize	= MIN_SIG_GEN_BUFFER_SIZE;
			unit->digitalPorts	= 0;
			break;

		case MODEL_PS3207B:
			unit->model			= MODEL_PS3207B;
			unit->sigGen		= SIGGEN_AWG;
			unit->firstRange	= PS3000A_50MV;
			unit->lastRange		= PS3000A_20V;
			unit->channelCount	= DUAL_SCOPE;
			unit->ETS			= TRUE;
			unit->AWGFileSize	= PS3206B_MAX_SIG_GEN_BUFFER_SIZE;
			unit->digitalPorts	= 0;
			break;

		default:
			printf("\nUnsupported device");
			exit(0);
		}
	}
}

/****************************************************************************
* DisplaySettings
* Displays information about the user configurable settings in this example
* Parameters
* - unit        pointer to the UNIT structure
*
* Returns       none
***************************************************************************/
void DisplaySettings()
{
	int ch;
	int voltage;

	printf("\n\nReadings will be scaled in (%s)\n", (scaleVoltages)? ("mV") : ("ADC counts"));

	for (ch = 0; ch < unitPico.channelCount; ch++)
	{
		if (!(unitPico.channelSettings[ch].enabled))
			printf("Channel %c Voltage Range = Off\n", 'A' + ch);
		else
		{
			voltage = inputRanges[unitPico.channelSettings[ch].range];
			printf("Channel %c Voltage Range = ", 'A' + ch);
			if (voltage < 1000)
				printf("%dmV\n", voltage);
			else
				printf("%dV\n", voltage / 1000);
		}
	}
	printf("\n");
}

/****************************************************************************
* DisableAnalogue - Disable Analogue Channels
****************************************************************************/
PICO_STATUS DisableAnalogue()
{
	PICO_STATUS status;
	short ch;

	// Turn off digital ports
	for (ch = 0; ch < unitPico.channelCount; ch++)
	{
		if((status = ps3000aSetChannel(unitPico.handle, (PS3000A_CHANNEL) ch, 0, PS3000A_DC, PS3000A_50MV, 0)) != PICO_OK)
			return status;
	}
	return status;
}


void activateTrigger(short mv, PS3000A_CHANNEL channel)
{
	short threshold =  mv_to_adc(mv, unitPico.channelSettings[channel].range);
	result = ps3000aSetSimpleTrigger(unitPico.handle, 1, channel, threshold, PS3000A_RISING, 0, 0);
	printf("Set trigger (%i) ********************** ", threshold);
	printError();
	printf("\n");
}

void desactivateTrigger(void)
{
	result = ps3000aSetSimpleTrigger(unitPico.handle, 0, 0, 0, PS3000A_RISING, 0, 0);
	printf("Desactivate trigger ********************** ");
	printError();
	printf("\n");
}

/****************************************************************************
*
* Select timebase, set oversample to on and time units as nano seconds
*
****************************************************************************/
void setTimebase()//voir docs pour correspondance timebase et temps entre deux échantillonages (0 = plus court => 1ns)
{
	int timeInterval;//temps en ns entre deux point
	int maxSamples;//nombre de points max dans une trace


	while (ps3000aGetTimebase(unitPico.handle, timebase, BUFFER_SIZE, &timeInterval, oversample, &maxSamples, 0))
	{
		timebase++;  // Increase timebase if the one specified can't be used.
	}

	printf("Sample Interval = %ins, max samples = %i points\n", timeInterval, maxSamples);
}

void tuneSampling(unsigned long tb)
{
	timebase = tb;
	oversample = 1;
}

void setChannelRange(PS3000A_CHANNEL channel, PS3000A_RANGE range)
{
	result = ps3000aSetChannel(unitPico.handle, channel, TRUE, PS3000A_DC, range, 0.0f);
	//result = ps3000aSetChannel(unitPico.handle, channel, TRUE, PS3000A_AC, range, 0.0f);
	if(result == PICO_OK)
	{
		unitPico.channelSettings[channel].enabled = TRUE;
		unitPico.channelSettings[channel].range = range;
	}

	printf("Set channel range ******************* ");
	printError();
	printf("\n");
}

void run()
{
	result = ps3000aRunBlock(unitPico.handle, 0, nbSamples, timebase, oversample, NULL, 0, NULL, NULL);
	//printf("Running ******************* ");
	//printError();
	//printf("\n");
}

void setNumberOfSamples(long samples)
{
	nbSamples = samples;
}

bool isReady()
{
	short b;
	result = ps3000aIsReady(unitPico.handle, &b);

	if(result == PICO_OK)
	{
		if(b != 0)
		{
			return TRUE;
		}
	}

	return FALSE;
}

//buffer should be of size nbSamples
void readData(short* buffer, PS3000A_CHANNEL channel)
{
	result = ps3000aSetDataBuffer(unitPico.handle, channel, buffer, nbSamples, 0, PS3000A_RATIO_MODE_NONE);

	if(result != PICO_OK)
	{
		printf("Error in read data (setDataBuffer): ");
		printError();
		printf("\n");
	}

	unsigned int samples = (unsigned int)nbSamples;
	result = ps3000aGetValues(unitPico.handle, 0, &samples, 1, PS3000A_RATIO_MODE_NONE, 0, NULL);

	if(samples != (unsigned int)nbSamples)
		printf("Obtained %i samples instead of %li.\n", samples, nbSamples);

	if(result != PICO_OK)
	{
		printf("Error in read data (getValues): ");
		printError();
		printf("\n");
	}
}

void readDataToCSV(PS3000A_CHANNEL channel, char* filename, int filename_size)
{
	//create buffer
	short* buffer = (short*)malloc(nbSamples*sizeof(short));

	//fill buffer
	readData(buffer, channel);

	//write to file
	char* extension = ".csv";
	char fileSpec[filename_size+5];
	snprintf( fileSpec, sizeof( fileSpec ), "%s%s", filename, extension );

	FILE* out;
	out = fopen(fileSpec, "w");
	long i = 0;

	if(nbSamples > 0)
		fprintf(out, "%i", buffer[i]);

	for(i = 1; i < nbSamples; i++)
	{
		fprintf(out, ",%i", buffer[i]);
	}


	fclose(out);
	free(buffer);
}

void appendDataToCSV(PS3000A_CHANNEL channel, FILE* out)
{
	//create buffer
	short* buffer = (short*)malloc(nbSamples*sizeof(short));

	//fill buffer
	readData(buffer, channel);

	long i = 0;

	if(nbSamples > 0)
		fprintf(out, "%i", buffer[i]);

	for(i = 1; i < nbSamples; i++)
	{
		fprintf(out, ",%i", buffer[i]);
	}
	fprintf(out, "\n");


	free(buffer);
}

void write_Binary_Header(FILE* out, int trace_count)//Write header: (size of trace (4 bytes))
{
  unsigned char buf[4];
  int i;
  for(i=0; i < 4; i++)
    buf[i] = (nbSamples >> (i*8)) & 0xFF;//LSB first

  fwrite(buf, 4, 1, out);

  for(i=0; i < 4; i++)
    buf[i] = (trace_count >> (i*8)) & 0xFF;//LSB first
  fwrite(buf, 4, 1, out);
}

void appendDataToBinary(PS3000A_CHANNEL channel, FILE* out)
{
	//create buffer
	short* buffer = (short*)malloc(nbSamples*sizeof(short));

	//fill buffer
	readData(buffer, channel);

	//write data array to file (Binary)
  /*int tot_written = 0;
  while(tot_written < nbSamples)
  {
    int w_size = 0;
    if(nbSamples*sizeof(short) > BUFSIZ)
    {
      w_size = BUFSIZ/sizeof(short);
    }
    else
    {
      w_size = nbSamples;
    }

    int wr = fwrite(buffer + tot_written, sizeof(short), w_size, out);
    tot_written += wr;
  }*/

	int i;
	for(i = 0; i < nbSamples; i++)
	{
		fwrite(&buffer[i], sizeof(short), 1, out);
	}


	free(buffer);
}
