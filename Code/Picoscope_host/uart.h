#ifndef UART_H
#define UART_H

//from stackoverflow

#include <errno.h>
#include <termios.h>
#include <unistd.h>

/*
The values for speed are B115200, B230400, B9600, B19200, B38400, B57600, B1200, B2400, B4800, etc.
The values for parity are 0 (meaning no parity), PARENB|PARODD (enable parity and use odd),
PARENB (enable parity and use even), PARENB|PARODD|CMSPAR (mark parity), and PARENB|CMSPAR (space parity).

"Blocking" sets whether a read() on the port waits for the specified number of characters to arrive.
Setting no blocking means that a read() returns however many characters are available without waiting for more, up to the buffer limit.
*/

int set_interface_attribs (int fd, int speed, int parity); //set uart parameters
void set_blocking (int fd, int should_block);//set blocking state
int readLineUART(int fd, char* buf, int max_size);//read until end of line
int readBlock(int fd, unsigned char* buf, int size);

//EXAMPLE *****************
/*
char *portname = "/dev/ttyUSB1"
 ...
int fd = open (portname, O_RDWR | O_NOCTTY | O_SYNC);
if (fd < 0)
{
        error_message ("error %d opening %s: %s", errno, portname, strerror (errno));
        return;
}

set_interface_attribs (fd, B115200, 0);  // set speed to 115,200 bps, 8n1 (no parity)
set_blocking (fd, 0);                // set no blocking

write (fd, "hello!\n", 7);           // send 7 character greeting

usleep ((7 + 25) * 100);             // sleep enough to transmit the 7 plus
                                     // receive 25:  approx 100 uS per char transmit
char buf [100];
int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
*/

#endif
