#include "picolib.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
#include <unistd.h>
#include "uart.h"

void fill_rand(unsigned char* buf, int size)
{
	int i;
	for(i=0;i<size;i++)
	{
		buf[i] = rand()%256;
	}
}

void to_hexa(unsigned char* hex_buf, int hex_buf_size, char *text, int text_max_size)
{
	if(text_max_size == 0)
		return;

	text[0] = '\0';
	char tmp[3];

	int i;
	for(i = 0; i < hex_buf_size; i++)
	{
		sprintf(tmp, "%02x", hex_buf[i]);
		if(strlen(tmp) + strlen(text) < text_max_size)
		{
			strcat(text, tmp);
		}

	}
}



int main()
{
	srand(time(NULL));
	printf("Starting...\n");
	printf("BUFSIZ = %d\n", BUFSIZ);


	int stm = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY /*| O_SYNC*/);
	if(stm < 0)
	{
		printf("Unable to connect to STM32.\n");
		exit(0);
	}

	set_interface_attribs (stm, B115200, 0);  // set speed to 115,200 bps, 8n1 (no parity)
	set_blocking (stm, 0);                // set blocking

	unsigned char key[16]={0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
	unsigned char plain[16];
	unsigned char cipher[16];
	char text[256];

	int tot = 1000000;

	//Pico config
	openPico();
	//active channel B: mesure tension (verticale)
	setChannelRange(PS3000A_CHANNEL_B, PS3000A_50MV );
	//setChannelRange(PS3000A_EXTERNAL, PS3000A_5V );
	//regle la resolution temporelle (valeur par défaut 1ns)
	tuneSampling(0);//1GS/s
	setTimebase();
	//une trace sera composée de n points
	setNumberOfSamples(5000);
	//regle le trigger: front montant, Channel EXT en millivolt
	activateTrigger(2000, PS3000A_EXTERNAL);

	//select round
	text[0] = 'r';
	text[1] = 1;
	write(stm, text, 2);

	//send key
	write(stm, "k", 1);
	write(stm, key, 16);

	readLineUART(stm, text, 256);
	printf("Key set result: %s\n", text);

  FILE *file_data;
	file_data = fopen("data.bin", "w");

	FILE *file_texts;
	file_texts = fopen("texts.bin", "w");

  printf("Running...\n");
	write_Binary_Header(file_data, tot);

  int i;

  for(i = 0; i < tot; i++)
  {
		run();

		fill_rand(plain, 16);
		write(stm, "f", 1);
		write(stm, plain, 16);


		int r = readBlock(stm, cipher, 16);
		//printf("Read %d.\n", r);

		//write texts
		to_hexa(plain, 16, text, 256);
		fprintf(file_texts, "%s\n", text);

		while(isReady() == FALSE);//wait for trigger

    appendDataToBinary(PS3000A_CHANNEL_B, file_data);
		//appendDataToCSV(PS3000A_CHANNEL_B, file_data);

		if(i%10 == 0)
		{
			printf("\rTrace %d/%d             ", i+1, tot);
			fflush(stdout);
		}

  }
	printf("\rTrace %d/%d\n", i, tot);
	printf("\n");

	fclose(file_texts);
  fclose(file_data);
	closePico();
	close(stm);

	printf("The end.\n");
	return 0;
};
