using MultivariateStats
using Base.LinAlg.BLAS
using Base.LinAlg

#------------------------------------------------------------------------------------------------------------------------
# Fonction qui lit un fichier binaire bini8 et le met dans une matrice M
# Chaque ligne est une traces (ligne = observation, colonne = variable)
#------------------------------------------------------------------------------------------------------------------------
function load_binary_matrix_i8(filepath)
	# ouverture du fichier
	f = open(filepath, "r")

	if(eof(f) == true)
		close(f)
	return 0
	end

	#lit le nombre de lignes
	trace_len::Int32 = read(f, Int32)
	#lit le nombre de colonne
	trace_count::Int32 = read(f, Int32)


  	M=zeros(Int8, trace_count, trace_len)
	i::Int32 = 1

  	while eof(f) == false && i <= trace_count
		M[i, :] = read(f, Int8, trace_len)
   	 	i+=1
  	end

	close(f)
	return M

end

function load_binary_matrix_i8(filepath, n_traces)
	# ouverture du fichier
	f = open(filepath, "r")

	if(eof(f) == true)
		close(f)
	   return 0
	end

	#lit le nombre de lignes
	trace_len::Int32 = read(f, Int32)
	#lit le nombre de colonne
	trace_count::Int32 = read(f, Int32)

  if(n_traces < trace_count)
    trace_count = n_traces
  end

  M=zeros(Int8, trace_count, trace_len)
	i::Int32 = 1

  	while eof(f) == false && i <= trace_count
		M[i, :] = read(f, Int8, trace_len)
   	 	i+=1
  	end

	close(f)
	return M

end


#------------------------------------------------------------------------------------------------------------------------
# Fonction qui lit un fichier binaire binf64 et le met dans une matrice M
#------------------------------------------------------------------------------------------------------------------------
function load_binary_matrix_f64(filepath::String)
	# ouverture du fichier
	f = open(filepath, "r")

	if(eof(f) == true)
		close(f)
	   return 0
	end

	#lit le nombre de lignes
	trace_len::Int32 = read(f, Int32)
	#lit le nombre de colonne
	trace_count::Int32 = read(f, Int32)


  	M=zeros(Float64, trace_count, trace_len)
	i::Int32 = 1

  	while eof(f) == false && i <= trace_count
		M[i, :] = read(f, Float64, trace_len)
   	 	i+=1
  	end

	close(f)
	return M

end

#------------------------------------------------------------------------------------------------------------------------
# Fonction qui lit un fichier binaire binf32 et le met dans une matrice M
#------------------------------------------------------------------------------------------------------------------------
function load_binary_matrix_f32(filepath)
	# ouverture du fichier
	f = open(filepath, "r")

	if(eof(f) == true)
		close(f)
	   return 0
	end

	#lit le nombre de lignes
	trace_len::Int32 = read(f, Int32)
	#lit le nombre de colonne
	trace_count::Int32 = read(f, Int32)


  	M=zeros(Float64, trace_count, trace_len)
	i::Int32 = 1

  	while eof(f) == false && i <= trace_count
		M[i, :] = read(f, Float32, trace_len)
   	 	i+=1
  	end

	close(f)
	return M

end

#------------------------------------------------------------------------------------------------------------------------
# Fonction qui écrit la matrice M dans un fichier binaire bin*
#------------------------------------------------------------------------------------------------------------------------
function save_binary_matrix(filepath, M)
	# ouverture du fichier
	f = open(filepath, "w")

	#ecrit le nombre de lignes
	trace_len::Int32 = size(M,2)
	write(f,trace_len)
	#ecrit le nombre de colonne
	trace_count::Int32 = size(M,1)
	write(f,trace_count)



  	for i = 1:trace_count
  		for j = 1:trace_len
  			write(f, M[i,j])
  		end
  		print("\r$i")
  	end
  	println("\r")

	close(f)

end

# Create PCA data from traces
# Take n traces for each element in K
function CreatePCAData(chemin_base,K,n,maxoutdim)
	fichier=string(chemin_base,"template$(K[1]).bin")#to get size
	Vec=load_binary_matrix_i8(fichier,1)
	p=size(Vec,2)
	N = size(K,2)
	M = round(Int8,zeros(p,n*N))#one trace one column to fit MultivariateStats lib

	start_ind = 1
	end_ind = n
	for i=1:N
		fichier=string(chemin_base,"template$(K[i]).bin")
		M[:,start_ind:end_ind] = load_binary_matrix_i8(fichier,n)'
		start_ind += n
		end_ind += n
	end
	println("Learning PCA data ($(p)*$(n*N))...")
	pca_data = fit(PCA, convert(Array{Float32},M), maxoutdim=maxoutdim)
  pca_path = string(chemin_base, "pca.raw")
  f_pca = open(pca_path, "w")
  serialize(f_pca, pca_data)
  close(f_pca)
  println("PCA data learned.")
  return pca_data
end

function load_pca(chemin_base)
	pca_path = string(chemin_base, "pca.raw")
  f_pca = open(pca_path, "r")
  pca_data = deserialize(f_pca)
  close(f_pca)
	return pca_data
end

#----------------------------------------------------------------------------------------------------
# fonction qui lit 1000 traces de taille t dans le fichier file et applique la transformation PCA
# file le fichier
# t nombre de points par ligne
# retourne une matrice m de taille
#----------------------------------------------------------------------------------------------------

function read1000_F32( file, t)
 	raw = zeros(1000, t)
	for i = 1:1000
		raw[i,:] = read(file, Float32, t)
	end
	raw
end

function read1000_I8( file, t)
 	raw = round(Int8, zeros(1000, t))
	for i = 1:1000
		raw[i,:] = read(file, Int8, t)
	end
	raw
end

function write_F32(file, M)
  n = size(M,1)
  p = size(M,2)
  for i = 1:n
    for j = 1:p
      write(file, M[i,j])
    end
  end
  end

#----------------------------------------------------------------------------------------------------
# fonction qui calcul la matrice de covariance et la moyenne
# version optimisée à partir pour un fichier BINF32 pour une matrice de taille n lignes n multiple de 1000
# retourne Sk matrice de covarience et mk vecteur moyenne (moyenne sur les lignes)
#----------------------------------------------------------------------------------------------------
function Covariance_opti_PCA(fichier)
	println("Calcul de covariance \n")

	#ouverture du fichier
	f = open(fichier, "r")
	if(eof(f) == true)
		println("Fichier vide")
		close(f)
		return 0
	end

	# t = nb colonne (temps)
	t = read(f, Int32)
	# n = nb ligne (taille campagne)
	n = read(f, Int32)

	if n%1000 != 0
		println("Le nombre de trace doit être un multiple de 1000")
		return 0
	end

	milliers = round(Int32, n/1000) #tombe juste

	# initialisation scatter matrix
	A = zeros(t,t)
	x_sums = zeros(t)

	for i = 1:milliers
		X = read1000_F32(f, t) # matrice de taille 1000*t
		axpy!(1.0, sum(X, 1), x_sums) #somme
		syrk!('L', 'T', 1.0, X, 1.0, A) # A <- A + X'X
		print("\r$(i*1000)")
	end
	println("\r")

	close(f)

	mk = x_sums / n         # mean for each variable
	Sk = (A - n * (mk*mk')) / (n - 1)

	for j=1:t, i=1:j
		Sk[i, j] = Sk[j, i]
	end

	return (Sk,mk')
end

function ModifyTemplates_PCA(chemin,K, pca_data)
  println("Modifying template traces from PCA data...")
  N=size(K,2)
	for i=1:N
		k=K[i]
		fname_in = string(chemin,"template$(k).bin")
    fname_out = string(chemin,"templatePCA$(k).binf32")
    fin = open(fname_in, "r")
    fout = open(fname_out, "w")

  	trace_len_in::Int32 = read(fin, Int32)
  	trace_count_in::Int32 = read(fin, Int32)

  	trace_len_out::Int32 = outdim(pca_data)
  	write(fout,trace_len_out)
  	trace_count_out::Int32 = trace_count_in
  	write(fout,trace_count_out)

    n_batches::Int32 = trace_count_in/1000

    for i in 1:n_batches
      raw = convert(Array{Float32}, read1000_I8(fin, trace_len_in))
      trans = convert(Array{Float32}, transform(pca_data, raw')')
      write_F32(fout,trans)
      print("\r$(i)/$(n_batches)")
    end

    close(fout)
    close(fin)
    println("\rtemplate$(k).bin modified to templatePCA$(k).binf32");
  end
end

function ModifyAttaque_PCA(chemin,K, pca_data)
  println("Modifying attaque traces from PCA data...")
  N=size(K,2)
	for i=1:N
		k=K[i]
		fname_in = string(chemin,"attaque$(k).bin")
    fname_out = string(chemin,"attaquePCA$(k).binf32")
    fin = open(fname_in, "r")
    fout = open(fname_out, "w")

  	trace_len_in::Int32 = read(fin, Int32)
  	trace_count_in::Int32 = read(fin, Int32)

  	trace_len_out::Int32 = outdim(pca_data)
  	write(fout,trace_len_out)
  	trace_count_out::Int32 = trace_count_in
  	write(fout,trace_count_out)

    n_batches::Int32 = trace_count_in/1000

    for i in 1:n_batches
      raw = convert(Array{Float32}, read1000_I8(fin, trace_len_in))
      trans = convert(Array{Float32}, transform(pca_data, raw')')
      write_F32(fout,trans)
      print("\r$(i)/$(n_batches)")
    end

    close(fout)
    close(fin)
    println("\rattaque$(k).bin modified to attaquePCA$(k).binf32");
  end
end

#----------------------------------------------------------------------------------------------------
# fonction qui calcule log(det(Sk)) en Passant par Cholesky
# Sk=C'C
#----------------------------------------------------------------------------------------------------
function CholeskyMethode(S)
	c=cholfact(S,:L,Val{true})
	mapreduce(x-> if x>0.0 log(x) else 0.0 end, +, diag(c[:L]))
end

#--------------------------------------------------------------
# fonction calcul les Template
#  K vecteur des hypothèse (pour un code pin 0 1 2 3 4 5 6 7 8 9)
# chemin ou sont stockées les courbes
#--------------------------------------------------------------
function PCATemplate_construction(chemin,K)
	N=size(K,2)
	for i=1:N
		k=K[i]
		fichier=string(chemin,"templatePCA$(k).binf32")
		(Sk,mk)=Covariance_opti_PCA(fichier)
		save_binary_matrix(string(chemin,"m$(k).binf64"), mk)
		Ak=CholeskyMethode(Sk)
		save_binary_matrix(string(chemin,"A$(k).binf64"),Ak)
		ISk=inv(Sk)
		save_binary_matrix(string(chemin,"IS$(k).binf64"),ISk)
		mk=0
		Sk=0
		ISk=0
		gc()
		gc()
	end
end


#--------------------------------------------------------------
#  fonction qui compare un vecteur aux différents templates
#  K vecteur des hypothèse (pour un code pin 0 1 2 3 4 5 6 7 8 9)
#  T vecteurs des hypothèses de traces obtenues
#  suppose que les templates sont déja contruits
#--------------------------------------------------------------
function Template_attack_chemin(cheminTemplate,cheminAttaque,K,T)

	taille_attaque=size(T,2)  # nombre de fichier à attaquer
	N=size(K,2)               # nombre de guesses


	Fdefine=0
	constante = 0
	n = 0
	F = 0
	p = 0

	for i1=1: N
		# chargement du Template
		k1=K[i1]
		println("Load template: ",k1)
		mk=load_binary_matrix_f64(string(cheminTemplate,"m$(k1).binf64"))
		mk=mk[1,:]
		A=load_binary_matrix_f64(string(cheminTemplate,"A$(k1).binf64"))
		A=A/2
		ISk=load_binary_matrix_f64(string(cheminTemplate,"IS$(k1).binf64"))


		for i2=1:taille_attaque
			#chargement du fichier attaque
			k2=T[i2]
			println("Load attaque: ",k2)
			fichier=string(cheminAttaque,"attaquePCA$(k2).binf32")
			Vec=load_binary_matrix_f32(fichier)

			if Fdefine==0
				n=size(Vec,1) # n nombre de traces
				p=size(Vec,2) # p nombre de points
				#constante
				constante=p*log(2*pi)/2
				F=zeros(taille_attaque,N,n)
				Fdefine=1
			end


			for i3=1:n
				# ouverture des traces d'attaque
				print("\r$(i3)/$(n)")
				v=Vec[i3,:]
				V=v-mk
				#formule V*ISk*V'   avec ligne, mat, col
				B=V'*symv('U',ISk,V)
				B=B/2
				R=-(A+B+constante)
				F[i2,i1,i3]=R[1]
				B=0
				gc()
				gc()
			end
			print("\r")
		end
		mk=0
		ISk=0
		gc()
		gc()

	end

	for i=1:taille_attaque
		k=T[i]
		Fk=reshape(F[i,:,:],N,n)
		save_binary_matrix(string(cheminAttaque,"F$(k).binf64"),Fk)
	end

	return F

end

#--------------------------------------------------------------
# fonction retourne le vecteurs d'indices des minimums des lignes
# F la matrice
#-------------------------------------------------------------------
function Mini(F)
	n=size(F,1)
	m=zeros(n)

	for i=1:n
		m[i]=indmin(F[i,:])
	end
	return m
end

#--------------------------------------------------------------
# fonction retourne le vecteurs d'indices des maximums des lignes
# f la matrice
#-------------------------------------------------------------------
function Maxi(F)

	n=size(F,1)
	m=zeros(n)

	for i=1:n
		m[i]=indmax(F[i,:])
	end


	return m
end

function max_rankings(F)
	n=size(F,1)
	v=size(F,2)
	m = zeros(Int32, n,v)

	for i=1:n
		m[i,:] = max_rank(copy(F[i,:]))
	end

	return m
end

function max_rank(v)
	s = size(v,1)
	res = zeros(Int32, s)

	for i=1:s
		res[i] = indmax(v)
		v[res[i]] = -Inf
	end

	res
end

function get_ranks(ranks, val)
	s = size(ranks, 1)
	res = zeros(Int32, s)

	for i=1:s
		res[i] = findfirst(ranks[i,:], val)
	end
	res
end


function plot_hist_ranks(ranks)
	n = size(ranks, 1)
	d = size(ranks, 2)

	p = 0

	for i=1:d
		h = fit(StatsBase.Histogram, get_ranks(ranks, i))

		if i > 1
			p = oplot(h.weights)
		else
			p = plot(1:d, h.weights)
		end
	end
	p
end

#--------------------------------------------------------------
# fonction retourne les résultats du template
# K vecteur des hypothèse (pour un code pin 0 1 2 3 4 5 6 7 8 9)
#--------------------------------------------------------------
function res_template_compte(chemin,K)
	N=size(K,2)
	res=zeros(N,N)
	for i=1:N
		k=K[i]
		F=load_binary_matrix_f64(string(chemin,"F$(k).binf64"))
		n=size(F,2)
		maxi=Maxi(F')-1
		#TODO convertir to K de manière générale
		for j=1:N
			k2=K[j]
			res[i,j]=count(x->x==k2, maxi)
		end
	end

	#normalisation en pourcentage
	k=K[1]
	F=load_binary_matrix_f64(string(chemin,"F$(k).binf64"))
	n=size(F,2)
	res_norm=res*100/n

	#taux de succès final
	Success_rate=sum(diag(res_norm))/N

	return (res,res_norm,Success_rate)

end





function template_complete_PCA(cheminTemplate,K,T,n_learning,maxoutdim)
	# create templates
  pca_data = CreatePCAData(cheminTemplate,K,n_learning,maxoutdim)
  ModifyTemplates_PCA(cheminTemplate,K, pca_data)#create templatePCA$(k).binf32
  PCATemplate_construction(cheminTemplate,K)#create m$(k).binf64, A$(k).binf64, IS$(k).binf64

	#now test attaques
	ModifyAttaque_PCA(cheminTemplate,T,pca_data)#create attaquePCA$(t).binf32
	Template_attack_chemin(cheminTemplate,cheminTemplate,K,T)#Attack!
	(res,res_norm,Success_rate) = res_template_compte(cheminTemplate,K)
	rvar= principalratio(pca_data)
	println("Preserved var ratio:", rvar)
	(res,res_norm,Success_rate)
end
