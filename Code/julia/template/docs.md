<!--
@Author: ronan
@Date:   24-11-2016
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 24-11-2016
-->



# How to use the template attack code with PCA

## Principles

The template attack is a based on supervised classification.
The secret is modified on the profiling device, a lot of data is measured for this secret to characterize the leakage.
Then the data measured on the attacked device is compared to the different classes generated during the profiling phase.

A preprocessing (PCA) is done for two reasons: eliminate floating point errors in the computations and reduced the trace size to something manageable with the RAM available.
The trace length is reduced with this preprocessing.

## Inputs

### Templates

To learn the template data, files **templateX.bin** must be generated where **X** is the secret value for this class (format is described in this document).
Each one of these files must contains a lot of traces (100 000 is a minimum).

### Attacks

Similarly **attaqueY.bin** files must be generated where **Y** is the candidate secret. In this files one or several (preferably several) traces are tested against the different template classes.

## Use

From the *template* and *attaque* files, the fully automatic analysis can be performed with the following code:
```
include("PCA_template.jl")
cheminTemplate="/home/[user]/traces/" # folder where templateX.bin and attaqueY.bin are located
K=[0 1 2 3 4 5 6 7 8 9] # possible secret values (X values)
T=K # candidate values used (Y values)
n_learning = 1000 #how many traces are taken from each templateX.bin to learn the variances in the PCA preprocessing. Approximately, n_learning*size(K,2) should be around 10000.
maxoutdim = 10000 #the maximum trace len after the PCA phase
(res,res_norm,Success_rate) = template_complete_PCA(cheminTemplate, K, T, n_learning, maxoutdim) # perform the analysis (take several hours)
```

The analysis can be decomposed in the following steps.
### Learning

Learning phase for the preprocessing:
```
pca_data = CreatePCAData(cheminTemplate,K,n_learning,maxoutdim)
```
Apply the preprocessing and generates new **templatePCAX.binf32** files:
```
ModifyTemplates_PCA(cheminTemplate,K, pca_data)
```
Learn the templates and generates **mX.binf64**, **AX.binf64**, **ISX.binf64** files:
```
PCATemplate_construction(cheminTemplate,K)
```
### Attack

Apply the preprocessing and generates new **attaquePCAY.binf32** files:
```
ModifyAttaque_PCA(cheminTemplate,T,pca_data)
```

Perform the analysis (the second *cheminTemplate* argument can be modified to point to a different folder where **attaqueY.bin** files are, change the previous path if it is the case):
```
Template_attack_chemin(cheminTemplate,cheminTemplate,K,T)
```

Computes stats to evaluate success rate:
```
(res,res_norm,Success_rate) = res_template_compte(chemin,K)
```


## Bin format

The *bin* format is a simple format to store matrices. The format is the following:
- Trace length on 4 bytes as an integer. This is the number of **samples** in each trace.
- Trace count on 4 bytes as an integer. This is the number of *traces* in the file.
- Trace 0 on *sample count*sample byte size* bytes.
- Trace 1 on *sample count*sample byte size* bytes.
- ...
- Trace [trace count - 1] on *sample count*sample byte size* bytes.

Samples can be of different types, specified in the file extension:
- **bini8** (or **bin**), samples are signed bytes
- **binf32**, samples are floats (32-bits)
- **binf64**, samples are doubles (64-bits)
- ...

Each sample is stored as its binary representation.
