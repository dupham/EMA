#------------------------------------------------------------------------------------------------------------------------
#
# Fichiers repertoriant les fonctions annexes utile aux attaques en template
# Ronan Lashermes et Hélène Le Bouder
#
#------------------------------------------------------------------------------------------------------------------------
using Base.LinAlg.BLAS
using Base.LinAlg

#------------------------------------------------------------------------------------------------------------------------
# Fonction qui lit un fichier binaire bini8 et le met dans une matrice M
#------------------------------------------------------------------------------------------------------------------------
function load_binary_matrix_i8(filepath)
	# ouverture du fichier
	f = open(filepath, "r")

	if(eof(f) == true)
		close(f)
	return 0
	end

	#lit le nombre de lignes
	trace_len::Int32 = read(f, Int32)
	#lit le nombre de colonne
	trace_count::Int32 = read(f, Int32)


  	M=zeros(Int8, trace_count, trace_len)
	i::Int32 = 1

  	while eof(f) == false && i <= trace_count
		M[i, :] = read(f, Int8, trace_len)
   	 	i+=1
  	end

	close(f)
	return M

end



#------------------------------------------------------------------------------------------------------------------------
# Fonction qui lit un fichier binaire binf64 et le met dans une matrice M
#------------------------------------------------------------------------------------------------------------------------
function load_binary_matrix_f64(filepath)
	# ouverture du fichier
	f = open(filepath, "r")

	if(eof(f) == true)
		close(f)
	return 0
	end

	#lit le nombre de lignes
	trace_len::Int32 = read(f, Int32)
	#lit le nombre de colonne
	trace_count::Int32 = read(f, Int32)


  	M=zeros(Float64, trace_count, trace_len)
	i::Int32 = 1

  	while eof(f) == false && i <= trace_count
		M[i, :] = read(f, Float64, trace_len)
   	 	i+=1
  	end

	close(f)
	return M

end

#------------------------------------------------------------------------------------------------------------------------
# Fonction qui écrit la matrice M dans un fichier binaire bin*
#------------------------------------------------------------------------------------------------------------------------
function save_binary_matrix(filepath, M)
	# ouverture du fichier
	f = open(filepath, "w")

	#ecrit le nombre de lignes
	trace_len::Int32 = size(M,2)
	write(f,trace_len)
	#ecrit le nombre de colonne
	trace_count::Int32 = size(M,1)
	write(f,trace_count)



  	for i = 1:trace_count
  		for j = 1:trace_len
  			write(f, M[i,j])
  		end
  		print("\r$i")
  	end
  	println("\r")

	close(f)

end

#----------------------------------------------------------------------------------------------------
# fonction qui lit 1000 traces de taille t dans le fichier file
# file le fichier
# t nombre de points par ligne
# retourne une matrice m de taille
#----------------------------------------------------------------------------------------------------

function read1000(file, t)
 	m = zeros(1000, t)
	for i = 1:1000
		m[i,:] = read(file, Int8, t)
	end
	return m
end

#----------------------------------------------------------------------------------------------------
# fonction qui calcul la matrice de covariance et la moyenne
# version optimisée à partir pour un fichier pour une matrice de taille n lignes n multiple de 1000
# retourne Sk matrice de covarience et mk vecteur moyenne (moyenne sur les lignes)
#----------------------------------------------------------------------------------------------------
function Covariance_opti(fichier)
	println("Calcul de covariance \n")

	#ouverture du fichier
	f = open(fichier, "r")
	if(eof(f) == true)
		println("Fichier vide")
		close(f)
	return 0
  	end

	# t = nb colonne (temps)
	t = read(f, Int32)
	# n = nb ligne (taille campagne)
	n = read(f, Int32)

	if n%1000 != 0
		println("Le nombre de trace doit être un multiple de 1000")
		return 0
	end

	milliers = round(Int32, n/1000) #tombe juste

	# initialisation scatter matrix
	A = zeros(t,t)
	x_sums = zeros(t)

	for i = 1:milliers
		X = read1000(f, t) # matrice de taille 1000*t
		axpy!(1.0, sum(X, 1), x_sums) #somme
		syrk!('L', 'T', 1.0, X, 1.0, A) # A <- A + X'X
		print("\r$(i*1000)")
	end
	println("\r")

	close(f)

	mk = x_sums / n         # mean for each variable
	Sk = (A - n * (mk*mk')) / (n - 1)

	for j=1:t, i=1:j
		Sk[i, j] = Sk[j, i]
	end

	return (Sk,mk')

end

#----------------------------------------------------------------------------------------------------
# fonction qui calcule log(det(Sk)) en Passant par Cholesky
# Sk=C'C
#----------------------------------------------------------------------------------------------------
function CholeskyMethode(Sk)

	#c=cholfact(Sk,pivot=true)
	c=cholfact(Sk,:L,Val{true})
	C=c[:L]
	diag=Diagonale(C)
	n=size(diag,1)*size(diag,2)
	d=0
	tmp=0

	for i=1:n
		a=diag[i]
		if a>0
			d=d+log(a)
		else
			if tmp!=0
				b=log(a*tmp)
				d=d+b
				tmp=0
			else
				tmp=a
			end
		end

		end
		d=2*d

	return d
end

function CholeskyMethode2(S)
	c=cholfact(S,:L,Val{true})
	mapreduce(x-> if x>0.0 log(x) else 0.0 end, +, diag(c[:L]))
end

#--------------------------------------------------------------
# fonction qui prends les éléments diagonaux d'une matrice carrée
#-------------------------------------------------------------------
function Diagonale(M)

	n=size(M,1)
	diag=zeros(n)

	tmp=1
	c=0

	for i=1:n

		diag[i]=M[i,i]

	end


	return diag
end





#--------------------------------------------------------------
# fonction retourne le vecteurs d'indices des minimums des lignes
# F la matrice
#-------------------------------------------------------------------
function Mini(F)

	n=size(F,1)
	m=zeros(n)

	for i=1:n

		m[i]=indmin(F[i,:])

	end


	return m
end

#--------------------------------------------------------------
# fonction retourne le vecteurs d'indices des maximums des lignes
# f la matrice
#-------------------------------------------------------------------
function Maxi(F)

	n=size(F,1)
	m=zeros(n)

	for i=1:n

		m[i]=indmax(F[i,:])

	end


	return m
end


#--------------------------------------------------------------
# fonction qui moyenne les traces
# T vecteurs du nombres de fichiers traces à moyennes
# b par combien faut il moyenner
#-------------------------------------------------------------------

function Moyennage_Traces(chemin,b,T)
	N=size(T,2)  # nombre de fichier à moyenner

	cheminAttaque=string(chemin,"Moyennage$(b)")
	for i=1:N
			#chargement du fichier original
			k=T[i]
			fichier=string(chemin,"attaque$(k).bin")
			Traces=load_binary_matrix_i8(fichier)

			Traces_moyennee=Moyennage(Traces,b)

			#creation fichier
			nomNewfichier=string(cheminAttaque,"attaque$(k).bin")
			Traces_moyennee=round(Int8,Traces_moyennee)
			save_binary_matrix(nomNewfichier, Traces_moyennee)


	end

	return cheminAttaque

end


#--------------------------------------------------------------
# fonction qui moyenne les traces
# T vecteurs du nombres de fichiers traces à moyennes
# b par combien faut il moyenner
#-------------------------------------------------------------------

function Moyennage_F(chemin,b,T)

	N=size(T,2)  # nombre de fichier à moyenner

	cheminMoyennee=string(chemin,"Moyennage$(b)")
	for i=1:N
			#chargement du fichier original
			k=T[i]
			fichier=string(chemin,"F$(k).binf64")
			Traces=load_binary_matrix_f64(fichier)

			Traces_moyennee=Moyennage(Traces,b)'

			#creation fichier
			nomNewfichier=string(cheminMoyennee,"F$(k).binf64")
			save_binary_matrix(nomNewfichier, Traces_moyennee)

	end

	return cheminMoyennee

end


#--------------------------------------------------------------
# fonction qui moyenne des lignes
# b par combien faut il moyenner
#-------------------------------------------------------------------

function Moyennage(M,b)

	n=size(M,1) # n nombre de lignes
	p=size(M,2) # p nombre de colonnes

	if mod(n,b)!=0
		print("Erreur, veuillez envoyer un diviseur du nombre de lignes \n")
		return 0
	end


	l=Int64(n/b)
	#Moyennage
	Moy=zeros(l,p)
	for j=1:l
		debut=(j-1)*b+1
		fin=(j-1)*b+b
		Moy[j,:]=mean(M[debut:fin,:],1)
	end

	return Moy



end
