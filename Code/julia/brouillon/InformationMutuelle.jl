#--------------------------------------------------------
# fonction qui calcule l'information mutuelle de 2 vecteurs
# X, Y vecteurs
# méthode par histogramme
#--------------------------------------------------------
function InformationMutuelle(V1,V2,arrondi)
	include("Entropy.jl")
	include("EntropyJointe.jl")
	
	# entropy 
	HX=Entropy(V1,arrondi)
	HY=Entropy(V2,arrondi)

	# entropy jointe
	H=EntropyJointe(V1,V2,arrondi)

	#information mutuelle
	MI= HX+HY-H

	return MI
end
