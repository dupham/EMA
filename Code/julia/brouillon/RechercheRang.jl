#--------------------------------------------------------------
# fonction qui charge les textes et les courbes
# O = nombre d'octets d'un PIN
# p = nombre de points
#--------------------------------------------------------------
function RechercheRang(val,V)
	i=1
	
	a=indmax(abs(V))
	
	while a!=val
		V[a]=0
		a=indmax(abs(V))
		i=i+1
	end
	
	return i


end
