#----------------------------------------------------------------------------------------------------
# fonction qui calcule qui calcule la moyenne sur les colonnes d'une matrice : Mk
#----------------------------------------------------------------------------------------------------
function Moyenne(Mk)

	mk=mean(Mk,1)
	return mk
end
		


#----------------------------------------------------------------------------------------------------
# fonction qui calcule qui calcule la moyenne sur les colonnes d'une matrice stockée dans un fichier binaire 
# sur le temps (colonnes) de manière optimisé en mémoire
#----------------------------------------------------------------------------------------------------
function Moyenne2(fichier::ASCIIString)

	#ouvre le fichier	
	f = open(fichier, "r")
 	if(eof(f) == true)
 		println("Fichier vide")
		close(f)
		return 0
	end	
	
	# t = nb colonne (temps)
	t= read(f, Int32)
	# n = nb ligne (taille campagne)
  	n= read(f, Int32)
	
	mk=zeros(1,t)'
	
	for i = 1 :n
		# récupération de la ligne i
		mk = read(f, Int8, t) + mk
	end
	mk=mk/n
		
	#ferme le fichier	
	close(f)

	return mk'
	
end

#----------------------------------------------------------------------------------------------------
# fonction qui calcule qui calcule la matrice de covariance d'une matrice : Mk
# sur le temps (colonnes)
#----------------------------------------------------------------------------------------------------
function Covariance(Mk)
	Sk=cov(Mk)
	return Sk
end

#----------------------------------------------------------------------------------------------------
# fonction qui calcule qui calcule la matrice de covariance d'une matrice stockée dans un fichier binaire 
# sur le temps (colonnes) de manière optimisé en mémoire
#----------------------------------------------------------------------------------------------------
function Covariance2(fichier::ASCIIString)

	#----------------------------------------------------------
	#Calcul de la moyenne sur les colonnes
	mk=Moyenne2(fichier)
	GG=mk'*mk

	#----------------------------------------------------------
	#ouvre le fichier	
	f = open(fichier, "r")
 	if(eof(f) == true)
 		println("Fichier vide")
		close(f)
		return 0
	end
	# t = nb colonne (temps)
	t= read(f, Int32)
	# n = nb ligne (taille campagne)
  	n= read(f, Int32)
	
	# initialisation matrice de covariance
	Sk=zeros(t,t)

	for i = 1 :n
		# récupération de la ligne i
		L = read(f, Int8, t)
		xdx=L*L'
		Sk=Sk+xdx
	end
	
	Sk=n/(n-1)*((1/n)*Sk-GG)

	#----------------------------------------------------------
	
	#ferme le fichier	
	close(f)
	return Sk
end


