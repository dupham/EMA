function sh(x)
	n = size(x,1)
	res = 0.0
	for i = 1:n
		if x[i] > 0
			res += x[i]*log(x[i])/log(2)
		end
	end
	return -res
end

function kl(x,y)
	n = size(x,1)
	res = 0.0
	for i =1:n
		if x[i] > 0
			res += x[i]*(log(x[i])-log(y[i]))/log(2)
		end
	end
	return res
end

function ce(x,y)
	sh(x) + kl(x,y)
end
