#--------------------------------------------------------
# fonction qui calcule la composante principale 
# m et t des vecteurs
#--------------------------------------------------------
function Principale_composante(m,t)
	
	if size(t,2)!=size(m,2)
		error("mettre des vecteurs colonnes de même tailles")
	end
	
	#on centre à la moyenene
	mu_m=mean(m)
	mu_t=mean(t)
	
	x=t-mu_t
	y=m-mu_m
	
	#on crée la matrice
	matrice=[y x]
	
	#matrice de covariance
	matcov=cov(matrice)
	
	#calcul des vecteurs propres et valeurs propres
	(Val,Vec)=eig(matcov)

	pca_val=maximum(Val)
	
	
	
	return pca_val 
end


#--------------------------------------------------------
# fonction qui réalise une PCA side channel classique
# M matrice de mesure
# P matrice de prédiction
# p nombre de points
#--------------------------------------------------------
function PCA(M,P,p)
	
	k=256
	pca=zeros(k,p)
	
	for i=1:k
		println("i= : ",i)
		t=P[:,i]
		for j=1:p
			
			m=M[:,j]
			pca[i,j]=Principale_composante(m,t)
		end
		
	end
	
	
		
		
	
	return pca 
end



#--------------------------------------------------------
# fonction qui réalise une PCA
# M matrice 
# V vecteur
# m taille de M
#--------------------------------------------------------
function PCA_matrice_vecteur(M,V,n)
	
	
	pca=zeros(n,1)
	
	for i=1:n
		
		m=M[i,:]
		pca[i]=Principale_composante(m',V')
		
		
	end
	
	
	return pca 
end
