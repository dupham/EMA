#--------------------------------------------------------
# fonction qui calcule les corrélations entre un vecteur et les colonnes d'une matrice
#--------------------------------------------------------
function correlation(M,v)
	
	
	n=size(v,1)
	m1=size(M,1)
	m2=size(M,2)
	
	C=zeros(Float64,m2)
	
		if n != m1
			error("erreur de dimension")
		end
		
	for i=1:m2
		#println("i= : ",i)
		C[i]= cor(v,M[:,i])[1]
	end

	return C

end
