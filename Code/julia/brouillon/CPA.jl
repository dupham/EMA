#--------------------------------------------------------
# fonction qui fait une cpa 
# M matrice de mesure
# P matrice de prédiction
# p nombres de points
#--------------------------------------------------------

function CPA(M,P,p)
	include("correlation.jl")
	
	C=zeros(256,p)
	
	for i=1:256
#		println("i= : ",i)l
		C[i,:]=correlation(M,P[:,i])
		
	end
	return C 
end
	
	
#--------------------------------------------------------
# fonction qui fait une cpa optimisee
# M matrice de mesure
# P matrice de prédiction
# n = taille campagne
# a revoir y a un bug
#--------------------------------------------------------

function CPA_opti(M,P,n)
	include("correlation.jl")
	
	X = [P[:,i] for i=1:256]
	TMP=pmap(x-> correlation(M,x), X)
	
	C=zeros(256,n)
	
	for i=1:256
		C[i,:]=TMP[i]
 	end
	
	return C 
end

#--------------------------------------------------------
# fonction qui recup le max
#
#--------------------------------------------------------
function CPA_Max(C)

	(a,b)=size(C)

	Maxi=float(zeros(a,1))	
	
	for i=1:a
		Maxi[i]=maximum(C[i,:])
	end

	K=indmax(Maxi)
end


