#--------------------------------------------------------------
# fonction qui renomme des fichiers
# Ronan Lashermes
#-----------------------------------------------------------

function rename_all()
	i = 1 #de fichier
  	for filename in readdir() #pour chaque fichier ou dossier dans le répertoire courant
    		if isfile(filename)#on garde que les fichiers, on ignore les dossier (version non récursive)
      		newname = "file$i.doc"
     		mv(filename, newname)
     		i=i+1
    	end
  end
end


#--------------------------------------------------------------
# fonction quiclear le terminal julia
#-----------------------------------------------------------

function clear()
    Base.run(`clear`)
    for var in names(Main)
        try
            eval(parse("$var=0"))
        catch e
        end
    end
    gc()
end
