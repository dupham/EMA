# @Author: Hélène Le Bouder, Ronan Lashermes
# @Date:   2016-2017
# @Email:  helene.le-bouder@inria.fr, ronan.lashermes@inria.fr
# @License: CC-by-nc


#--------------------------------------------------------
# tools pour MIA
#--------------------------------------------------------

#---------------------------------------------------------------------------------------
# Function : mutual_information
# compute the mutual information a matrix M (traces : text*times)
# and a P matrix of predictions (text* predictions )
#---------------------------------------------------------------------------------------
function  mutual_information_analysis(M,P)
	if size(M,1)!=size(P,1)
		error("error of dimension")
	end

	#nb of texts
	n=size(M,2)

	# nb of points
	p=size(M,2)
	# nb of predictions (k=256)
	k=size(P,2)

	mia=zeros(p,k)

	I=round.(Int64,1:p)
	J=round.(Int64,1:k)

	#map( (i,j) -> mia[i,j]=mutual_information_to_mia(M[:,i],P[:,j],n), (I,J))

	  #for i=1:p
		#for j=1:k
		#	mia[i,j]=mutual_information_to_mia(M[:,i],P[:,j],n)
		#end
		#println("i=$(i)")
		#mia[i,:]=map( j -> mutual_information_to_mia(M[:,i],P[:,j],n), J)

	#end

	map(i -> mia[i,:]= mutual_information_inter(M[:,i],P,J,n),I)

	return mia

end
#---------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------
# Function : mutual_information_inter
# compute the mutual information a matrix P and a vector M_inter
#---------------------------------------------------------------------------------------
function  mutual_information_inter(M_inter,P,J,n)

	mia_inter=map( j -> mutual_information_to_mia(M_inter,P[:,j],n), J)
	print("+")
	return mia_inter

end
#---------------------------------------------------------------------------------------





#---------------------------------------------------------------------------------------
# Function : mutual_information_to_mia
# compute the mutual information between 2 vectors X and Y of size n
#---------------------------------------------------------------------------------------
function  mutual_information_to_mia(X,Y,n)

	(histxy,histx,histy)=histogramm_to_mia(X,Y,n)

	# entropy
	HX=Entropy_to_mia(histx,n)
	HY=Entropy_to_mia(histy,n)

	# entropy jointe
	H=Entropy_to_mia(histxy,n)

	#information mutuelle
	MI= HX+HY-H

	return MI
end
#---------------------------------------------------------------------------------------


#--------------------------------------------------------
# fonction : histogramm on a pairs
# X Y vectors of same size : n
# build the histogramms of values
#--------------------------------------------------------
function histogramm_to_mia(X,Y,n)

	histxy = Dict()
	histx = Dict()
	histy = Dict()

	#hist
	for i=1:n
		x = X[i]
		#ajoute t au dictionnaire
		val = get(histx, x, 0)
		histx[x] = val+1

		y = Y[i]
		#ajoute t au dictionnaire
		val = get(histy, y, 0)
		histy[y] = val+1

		xy = (x,y)
		#ajoute t au dictionnaire
		val = get(histxy, xy, 0)
		histxy[xy] = val+1
	end

	return (histxy,histx,histy)
end
#--------------------------------------------------------

#--------------------------------------------------------
# fonction : Entropy_to_mia
# hist an histogramm
# use histogramm
#--------------------------------------------------------

function Entropy_to_mia(hist,n)

	entropy = 0.0
	#calcule entropie à partir du compteur
	for x in keys(hist)
		p = hist[x] / n
		entropy = entropy + p*log2(p)
	end

	return -entropy
end
#--------------------------------------------------------



#--------------------------------------------------------
# Fonctions moins optimales mais qui peuvent toujours servir
#--------------------------------------------------------

include("Outils.jl")

#---------------------------------------------------------------------------------------
# Function : mutual_information
# compute the mutual information between 2 vectors X and Y
# computation classic
#---------------------------------------------------------------------------------------
function  mutual_information(X,Y)

	# entropy
	HX=Entropy(X)
	HY=Entropy(Y)

	# entropy jointe
	H=Entropy_Jointe(X,Y)

	#information mutuelle
	MI= HX+HY-H

	return MI
end
#---------------------------------------------------------------------------------------

#--------------------------------------------------------
# fonction : Entropy
# X vector
# use histogramm
#--------------------------------------------------------

function Entropy(X)
	n = size(X,1)*size(X,2)
	#histogramme
	hist=histogramm(X)

	entropy = 0.0
	#calcule entropie à partir du compteur
	for x in keys(hist)
		p = hist[x] / n
		entropy = entropy + p*log2(p)
	end

	return -entropy
end
#--------------------------------------------------------

#--------------------------------------------------------
# fonction : histogramm
# X vector
# build the histogramm of values X
#--------------------------------------------------------
function histogramm(X)
	n = size(X,1)*size(X,2)
	hist = Dict()

	for i=1:n
		x = X[i]
	#	#add t to dictionary
		val = get(hist, x, 0)
		hist[x] = val+1
	end


	return hist
end
#--------------------------------------------------------


#--------------------------------------------------------
# fonction Entropy_Jointe
# X Y vectors of same size
# use histogramm
#--------------------------------------------------------
function Entropy_Jointe(X,Y)
	n1 = size(X,1)*size(X,2)
	n2 = size(Y,1)*size(Y,2)

	if (n1!=n2)
    		error("size error");
	end

	histogramm2(X,Y,n1)
	entropy = 0.0

	#calcule entropie à partir du compteur
	for k in keys(dico)
		p = dico[k] / n1
		entropy = entropy + p*log2(p)
	end

	return -entropy
end
#--------------------------------------------------------

#--------------------------------------------------------
# fonction : histogramm on a pairs
# X Y vectors of same size : n
# build the histogramm of values X
#--------------------------------------------------------
function histogramm2(X,Y,n)

	hist = Dict()

	#hist
	for i=1:n
		xy = (X[i],Y[i])
		#ajoute t au dictionnaire
		val = get(hist, xy, 0)
		hist[xy] = val+1
	end

	return hist
end
