# @Author: Hélène Le Bouder, Ronan Lashermes
# @Date:   2016-2017
# @Email:  helene.le-bouder@inria.fr, ronan.lashermes@inria.fr
# @License: CC-by-nc


include("Prediction.jl")
include("Distingueurs.jl")
include("mia.jl")

#----------------------------------------------------------------------------------------------------
# fonction qui lit 1000 traces de taille t dans le fichier file
# file le fichier
# t nombre de points par ligne
# retourne une matrice m de taille
#----------------------------------------------------------------------------------------------------

function read_batch_mean(file, t, batch, me)
 	m = zeros(batch, t)
	for i = 1:batch
		m[i,:] = read(file, Int8, t)-me
	end
	return m
end

function read_batch(file, t, batch)
 	m = zeros(batch, t)
	for i = 1:batch
		m[i,:] = read(file, Int8, t)
	end
	return m
end

function big_mean(data_path)
	batch = 10000
	#ouverture du fichier
	f = open(data_path, "r")
	if(eof(f) == true)
		println("Fichier vide")
		close(f)
		return 0
	end

	# t = nb colonne (temps)
	t = read(f, Int32)
	# n = nb ligne (taille campagne)
	n = read(f, Int32)

	me = zeros(t)

	println("$n traces of length $t")

	if n%batch != 0
		println("Le nombre de trace doit être un multiple de $(batch)")
		return 0
	end

	n_batches = round.(Int32, n/batch) #tombe juste

	for i = 1:n_batches
		X = read_batch(f, t, batch) # matrice de taille batch*t

		me += mean(X,1)[:]

		print("\r$(i*100/n_batches)%")
	end

	me /= n_batches

	println("\r")
	close(f)

	me
end

# Big CPA for big files to big to be loaded
function big_cpa(data_path, P)
	batch = 10000

	mD = big_mean(data_path)

	#ouverture du fichier
	f = open(data_path, "r")
	if(eof(f) == true)
		println("Fichier vide")
		close(f)
		return 0
	end


	mP = mean(P,1)[:]
	mPsub = hcat(fill(mP, batch)...)'

	# t = nb colonne (temps)
	t = read(f, Int32)
	# n = nb ligne (taille campagne)
	n = read(f, Int32)

	k = size(P,2)

	println("$n traces of length $t")

	# temp CPA vectors
	sx = zeros(t)
	sy = zeros(k)
	sxy = zeros(t,k)
	sx2 = zeros(t)
	sy2 = zeros(k)

	if n%batch != 0
		println("Le nombre de trace doit être un multiple de $(batch)")
		return 0
	end

	n_batches = round.(Int32, n/batch) #tombe juste

	for i = 1:n_batches
		X = read_batch_mean(f, t, batch, mD) # matrice de taille batch*t
		Y = P[(i-1)*batch+1:i*batch,:] - mPsub

		for v in 1:k
			sy[v] += sum(Y[:,v])
			sy2[v] += sum(dot(Y[:,v],Y[:,v]))
		end

		for u in 1:t
			sx[u] += sum(X[:,u])
			sx2[u] += sum(dot(X[:,u],X[:,u]))
			for v in 1:k
				sxy[u,v] += sum(dot(X[:,u], Y[:,v]))
			end
		end

		print("\r$(i*100/n_batches)%")
	end
	println("\r")
	close(f)

	cpa = zeros(t,k)

	for u in 1:t
		for v in 1:k
			cpa[u,v] = (n*sxy[u,v]-sx[u]*sy[v])/(sqrt((n*sx2[u]-sx[u]*sx[u])*(n*sy2[v]-sy[v]*sy[v])))
		end
		print("\r$u/$t")
	end
	println("\r")

	return cpa
end

#--------------------------------------------------------
# Attacks  Correlation Power Analysis CPA
#--------------------------------------------------------
# On the first round on the bytes
#--------------------------------------------------------

#--------------------------------------------------------
# Attack  CPA_first_round_one_byte
# T texts
# M traces
# o byte
# return K
#--------------------------------------------------------
function CPA_first_round_one_byte(T,M,o)

	P=Predictions_first_round_byte(T,o)
	cpa=correlation(M,P)
	# cpa_savefig(cpa,o,0)
	k=CPA_K(cpa)

	# return k
	return cpa
end
#--------------------------------------------------------

#--------------------------------------------------------
# Attack  CPA_first_round_one_byte
# T texts
# M traces
# byte
# return K best guess round key
#--------------------------------------------------------
function CPA_first_round_bytes(T,M)

	O=round.(Int64,1:16)
	K=pmap(o ->CPA_first_round_one_byte(T,M,o),O)
	return K

end
#--------------------------------------------------------

#--------------------------------------------------------
# On the first round on the bits
# It is not a real attack on bits,
# It is to each byte to see the difference leakage on each bits
#--------------------------------------------------------


#--------------------------------------------------------
# Attack  CPA_first_round_one_bit
# T texts
# M traces
# o byte
# b bit
# return K
#--------------------------------------------------------
function CPA_first_round_one_bit(T,M,o,b)

	P=Predictions_first_round_bit(T,o,b)
	cpa=correlation(M,P)
	#cpa_savefig(cpa,o,b) # affichage
	k=CPA_K(cpa)

	return k
end
#--------------------------------------------------------


#--------------------------------------------------------
# Attack  CPA_first_round_bits
# T texts
# M traces
# o byte
# return the best guesses according the 8 bits of the byte o
#--------------------------------------------------------
function CPA_first_round_bits(T,M,o)
	B=round.(Int64,1:8)
	K=map(b ->CPA_first_round_one_bit(T,M,o,b),B)
	return K
end
#--------------------------------------------------------

#--------------------------------------------------------
# Attack  CPA_first_round_all_bits
# T texts
# M traces
# byte
# return K best guess round key
#--------------------------------------------------------
function CPA_first_round_all_bits(T,M)

	O=round.(Int64,1:16)
	K=pmap(o ->CPA_first_round_bits(T,M,o),O)
	return K

end
#--------------------------------------------------------

#--------------------------------------------------------
# On the first last on the bytes
#--------------------------------------------------------

#--------------------------------------------------------
# Attack  CPA_last_round_one_byte
# T texts
# M traces
# o byte
# return K
#--------------------------------------------------------
function CPA_last_round_one_byte(T,M,o)

	P=Predictions_last_round_byte(T,o)
	cpa=correlation(M,P)
	cpa_savefig(cpa,o,0)
	k=CPA_K(cpa)

	return k
end
#--------------------------------------------------------

#--------------------------------------------------------
# Attack  CPA_last_round_one_byte
# T texts
# M traces
# byte
# return K best guess round key
#--------------------------------------------------------
function CPA_last_round_bytes(T,M,o)

	O=round.(Int64,1:16)
	K=pmap(o ->CPA_last_round_one_byte(T,M,o),O)
	return K

end
#--------------------------------------------------------

#--------------------------------------------------------
# On the first round on the bits
# It is not a real attack on bits,
# It is to each byte to see the difference leakage on each bits
#--------------------------------------------------------

#--------------------------------------------------------
# Attack  CPA_last_round_one_bit
# T texts
# M traces
# o byte
# b bit
# return K
#--------------------------------------------------------
function CPA_last_round_one_bit(T,M,o,b)

	P=Predictions_last_round_bit(T,o,b)
	cpa=correlation(M,P)
	#cpa_savefig(cpa,o,b) # affichage
	k=CPA_K(cpa)

	return k
end
#--------------------------------------------------------


#--------------------------------------------------------
# Attack  CPA_last_round_bits
# T texts
# M traces
# o byte
# return the best guesses according the 8 bits of the byte o
#--------------------------------------------------------
function CPA_last_round_bits(T,M,o)
	B=round.(Int64,1:8)
	K=map(b ->CPA_last_round_one_bit(T,M,o,b),B)
	return K
end
#--------------------------------------------------------


#--------------------------------------------------------
# Attack  CPA_last_round_all_bits
# T texts
# M traces
# byte
# return K best guess round key
#--------------------------------------------------------
function CPA_last_round_all_bits(T,M)

	O=round.(Int64,1:16)
	K=pmap(o ->CPA_last_round_bits(T,M,o),O)
	return K
end
#--------------------------------------------------------




#--------------------------------------------------------
# Attacks Mutual Information Analysis MIA
#--------------------------------------------------------
# On the first round on the bytes
#--------------------------------------------------------

#--------------------------------------------------------
# Attack  MIA_first_round_one_byte
# T texts
# M traces
# o byte
# return K
#--------------------------------------------------------
function MIA_first_round_one_byte(T,M,o)

	P=Predictions_first_round_byte(T,o)
	mia=mutual_information_analysis(M,P)
	mia_savefig(mia,o)
	k=CPA_K(mia)

	return k
end
#--------------------------------------------------------

#--------------------------------------------------------
# Attack  CPA_first_round_one_byte
# T texts
# M traces
# byte
# return K best guess round key
#--------------------------------------------------------
function MIA_first_round_bytes(T,M)

	O=round.(Int64,1:16)
	K=pmap(o ->MIA_first_round_one_byte(T,M,o),O)
	return K

end
#--------------------------------------------------------


#--------------------------------------------------------
# On the last round on the bytes
#--------------------------------------------------------

#--------------------------------------------------------
# Attack  MIA_last_round_one_byte
# T texts
# M traces
# o byte
# return K
#--------------------------------------------------------
function MIA_last_round_one_byte(T,M,o)

	P=Predictions_last_round_byte(T,o)
	mia=mutual_information_analysis(M,P)
	mia_savefig(mia,o)
	k=CPA_K(mia)

	return k
end
#--------------------------------------------------------

#--------------------------------------------------------
# Attack  CPA_last_round_one_byte
# T texts
# M traces
# byte
# return K best guess round key
#--------------------------------------------------------
function MIA_last_round_bytes(T,M)

	O=round.(Int64,1:16)
	K=pmap(o ->MIA_last_round_one_byte(T,M,o),O)
	return K

end
#--------------------------------------------------------
















#--------------------------------------------------------
# Attacks Principal Component Analysis PCA
#--------------------------------------------------------
# On the first round on the bytes
#--------------------------------------------------------

#--------------------------------------------------------
# Attack  PCA_first_round_one_byte
# T texts
# M traces
# o byte
# return K
#--------------------------------------------------------
function PCA_first_round_one_byte(T,M,o)

	P=Predictions_first_round_byte(T,o)
	pca=princical_component_analysis(M,P)
	pca_savefig(pca,o)
	k=indmax(pca)

	return k
end
#--------------------------------------------------------

#--------------------------------------------------------
# Attack  CPA_first_round_one_byte
# T texts
# M traces
# byte
# return K best guess round key
#--------------------------------------------------------
function PCA_first_round_bytes(T,M)

	O=round.(Int64,1:16)
	K=pmap(o ->PCA_first_round_one_byte(T,M,o),O)
	return K

end
#--------------------------------------------------------


#--------------------------------------------------------
# On the last round on the bytes
#--------------------------------------------------------

#--------------------------------------------------------
# Attack  PCA_last_round_one_byte
# T texts
# M traces
# o byte
# return K
#--------------------------------------------------------
function PCA_last_round_one_byte(T,M,o)

	P=Predictions_last_round_byte(T,o)
	pca=princical_component_analysis(M,P)
	pca_savefig(pca,o)
	k=indmax(pca)-1

	return k
end
#--------------------------------------------------------

#--------------------------------------------------------
# Attack  CPA_last_round_one_byte
# T texts
# M traces
# byte
# return K best guess round key
#--------------------------------------------------------
function PCA_last_round_bytes(T,M)

	O=round.(Int64,1:16)
	K=pmap(o ->PCA_last_round_one_byte(T,M,o),O)
	return K
end
#--------------------------------------------------------
