# @Author: Hélène Le Bouder, Ronan Lashermes
# @Date:   2016-2017
# @Email:  helene.le-bouder@inria.fr, ronan.lashermes@inria.fr
# @License: CC-by-nc

# using Winston
using Plots

#--------------------------------------------------------
#tools for CPA
#--------------------------------------------------------

#---------------------------------------------------------------------------------------
# Function : Correlation
# compute the correlation between a matrix M (traces) and a P matrix of predictions
#---------------------------------------------------------------------------------------
function correlation(M,P)

	if(size(M,1)!=size(P,1))
		error("Dimension error")
	end

	cpa=cor(M,P)

	return cpa

end
#---------------------------------------------------------------------------------------


#---------------------------------------------------------------------------------------
# Function : cpa_savefig
# print results and store the traces
# cpa results
# o targeted byte
# b targeted bit if attacks on bits else b=0
#---------------------------------------------------------------------------------------
function cpa_savefig(cpa,o,b)

	plot(cpa)

	ylabel("correlation")
	xlabel("times")

	if b!=0
		savefig("byte$(o)_bit$(b).pdf")
	else
		savefig("byte$(o).pdf")
	end

	return 0

end
#---------------------------------------------------------------------------------------


#---------------------------------------------------------------------------------------
# Function : CPA_K
# return the best guess K
# work for mia too
#---------------------------------------------------------------------------------------
function CPA_K(cpa)
	n=size(cpa,2)
	I=round.(Int64,1:n)
	maxi=map(i-> maximum(cpa[:,i]) ,I)
	k=indmax(maxi)-1
	return k

end
#---------------------------------------------------------------------------------------


#--------------------------------------------------------
#tools for MIA
#--------------------------------------------------------
include("mia.jl")

#---------------------------------------------------------------------------------------
# Function : mia_savefig
# print results and store the traces
# mia results
# o targeted byte
#---------------------------------------------------------------------------------------
# function mia_savefig(mia,o)
#
# 	plot(mia)
#
# 	ylabel("mutual information")
# 	xlabel("times")
#
# 	savefig("byte$(o).pdf")
#
# 	return 0
#
# end
#---------------------------------------------------------------------------------------


#--------------------------------------------------------
#tools for PCA
#--------------------------------------------------------
include("pca.jl")


#---------------------------------------------------------------------------------------
# Function : mia_savefig
# print results and store the traces
# mia results
# o targeted byte
#---------------------------------------------------------------------------------------
function pca_savefig(pca,o)

	plot(pca,"+")

	ylabel("principal component")
	xlabel("guesses")

	savefig("byte$(o).pdf")

	return 0

end
#---------------------------------------------------------------------------------------
