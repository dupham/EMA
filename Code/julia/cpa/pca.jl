# @Author: Hélène Le Bouder, Ronan Lashermes
# @Date:   2016-2017
# @Email:  helene.le-bouder@inria.fr, ronan.lashermes@inria.fr
# @License: CC-by-nc

include("Outils.jl")

#--------------------------------------------------------
# tools for PCA 
#--------------------------------------------------------

#---------------------------------------------------------------------------------------
# Function : princical_component_analysis
# compute the mutual information a matrix M (traces : text*times)
# and a P matrix of predictions (text* predictions )
#---------------------------------------------------------------------------------------
function princical_component_analysis(M,P)
	if size(M,1)!=size(P,1)
		error("error of dimension")
	end
	
	# nb of predictions (nk=256)
	nk=size(P,2)
	# mean of M on the column (for each times)
	mu_M=mean(M,1)

	#for k=1:nk
	#	println("k=$(k)")
	#	X=P[:,k]
	#	Wk=(compute_Wk_partitioning(M,mu_M,X))
	#	Sk=cov(Wk)
	#	(Val,Vec)=eig(Sk)
	#	pca[k]=maximum(Val)
	#end
	
	K=I=round(Int64,1:nk)
	pca=map(k->pca_k(M,P,mu_M,k),K)
	
	
	return pca 
end
#---------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------
# Function : pca_k
# map the loop for on the guess k
#---------------------------------------------------------------------------------------
function pca_k(M,P,mu_M,k )
	#println("k=$(k)")
	
	#for each prediction
	X=P[:,k]
	Wk=(compute_Wk_partitioning(M,mu_M,X))
	#covariance matrix of Wk
	Sk=cov(Wk)
	(Val,Vec)=eig(Sk)
	# the pca is the maximal eigen value
	pca=maximum(Val)
	return pca
end
#---------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------
# Function : compute_Wk_partitioning
# compute the matrix Wk of partitioning according values of vector X
# matrix M (traces : text*times) , mu_M means of M
#---------------------------------------------------------------------------------------
function  compute_Wk_partitioning(M,mu_M,X)

	#partitioning (often on HW)
	hist=histogramm(X)
	Wk=vcat(map(hw -> compute_wk(M,mu_M,X,hw), keys(hist))...)

	return Wk

end
#---------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------
# Function : compute_wk
# to make the map of the loop on different prediction values.
#---------------------------------------------------------------------------------------
function  compute_wk(M,mu_M,X,hw)
	
	index=find(x -> x == hw,X)
	M_hw=M[index,:]
	mu_M_hw=mean(M_hw,1)
	wk=mu_M_hw-mu_M

	return wk

end
#---------------------------------------------------------------------------------------



#--------------------------------------------------------
# function : princical_component_vector
# compute the princical_component between two vectors X,Y of same size
# (bonus)
#--------------------------------------------------------
function princical_component_vector(X,Y)
		
	
	if n!=n2
		error("error size vectors")
	end
	
	#center on the mean
	mu_x=mean(X)
	mu_y=mean(Y)
	
	x=X-mu_x
	y=Y-mu_y
	
	#matrix
	Mk=[y x]
	
	#covariance
	Sk=cov(matrice)
	
	#compute eigen values and vectors
	(Val,Vec)=eig(matcov)

	pca_val=maximum(Val)

	return pca_val 
end
#--------------------------------------------------------


