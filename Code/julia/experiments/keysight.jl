# @Author: Hélène Le Bouder, Ronan Lashermes
# @Date:   2016-2017
# @Email:  helene.le-bouder@inria.fr, ronan.lashermes@inria.fr
# @License: CC-by-nc

function connect_KS()
  connect(ip"131.254.13.161", 5025)
end

function get_traces(nb_traces)
  conn = connect_KS()

  # send command to get waveform data

  write(conn, ":WAV:FORM BYTE\n")
  write(conn, ":WAV:DATA?\n")

  # read waveform data
  sharp = String(read(conn, 2))

  if sharp[1] != '#'
    println("Error, data must start with '#', got $(sharp[1]) instead")
    return
  end
 # read number of digits in waveform_size
 size_digit_count = parse(Int, sharp[2])
 #read waveform size
 waveform_size = parse(Int, String(read(conn, size_digit_count)) )
 #read waveform
 waveform = read(conn, Int8, waveform_size)
 read(conn, 1)#trailing '\n'

 reshape(waveform, Int(waveform_size/nb_traces), nb_traces)'
end
