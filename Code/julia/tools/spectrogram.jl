# PyPlot includes a specgram function, but let's use the native
# implementation from DSP.jl. The function below extracts a
# spectrogram with standard parameters for speech (25ms Hanning windows
# and 10ms overlap), then plots it and returns it .
# fs is Sampling rate

using DSP
using PyPlot

function plot_spectrogram(s, fs)

    hwindow = 25e-6
    overlap = 10e-6
    S = spectrogram(s[:], round(Int, hwindow*fs),
                    round(Int, overlap*fs); window=hanning)
    t = time(S)
    f = freq(S)
    imshow(flipdim(log10(power(S)), 1), extent=[first(t), last(t),
             fs*first(f), fs*last(f)], aspect="auto")
    S
end
