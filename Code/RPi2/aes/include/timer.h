/**
* @Author: kevin, ronan
* @Date:   02-09-2016
* @Email:  sebanjila.bukasa@inria.fr, ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 05-09-2016
* @License: GPL
*/

#ifndef TIMER_H
#define TIMER_H

void timer_init(void);
unsigned long get_timer_tick(void);
void wait_us(unsigned long);

#endif
