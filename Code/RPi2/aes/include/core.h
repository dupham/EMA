/**
* @Author: kevin
* @Date:   21-10-2016
* @Email:  sebanjila.bukasa@inria.fr
* @Last modified by:   kevin
* @Last modified time: 21-10-2016
* @License: GPL
*/

#ifndef CORE_H
#define CORE_H

// MPIDR values for each cores

#define CORE0 0x80000F00
#define CORE1 0x80000F01
#define CORE2 0x80000F02
#define CORE3 0x80000F03

// Mailboxes to set values

#define COREMBXSET 0x40000080
#define MBXSETC00  (COREMBXSET)
#define MBXSETC01  (COREMBXSET + 0x4)
#define MBXSETC02  (COREMBXSET + 0x8)
#define MBXSETC03  (COREMBXSET + 0xC)

#define MBXSETC10  (COREMBXSET + 0x10)
#define MBXSETC11  (COREMBXSET + 0x14)
#define MBXSETC12  (COREMBXSET + 0x18)
#define MBXSETC13  (COREMBXSET + 0x1C)

#define MBXSETC20  (COREMBXSET + 0x20)
#define MBXSETC21  (COREMBXSET + 0x24)
#define MBXSETC22  (COREMBXSET + 0x28)
#define MBXSETC23  (COREMBXSET + 0x2C)

#define MBXSETC30  (COREMBXSET + 0x30)
#define MBXSETC31  (COREMBXSET + 0x34)
#define MBXSETC32  (COREMBXSET + 0x38)
#define MBXSETC33  (COREMBXSET + 0x3C)

// Mailboxes to read or clear values

#define COREMBXRDCLR 0x400000C0
#define MBXRDCLRC00  (COREMBXRDCLR)
#define MBXRDCLRC01  (COREMBXRDCLR + 0x4)
#define MBXRDCLRC02  (COREMBXRDCLR + 0x8)
#define MBXRDCLRC03  (COREMBXRDCLR + 0xC)

#define MBXRDCLRC10  (COREMBXRDCLR + 0x10)
#define MBXRDCLRC11  (COREMBXRDCLR + 0x14)
#define MBXRDCLRC12  (COREMBXRDCLR + 0x18)
#define MBXRDCLRC13  (COREMBXRDCLR + 0x1C)

#define MBXRDCLRC20  (COREMBXRDCLR + 0x20)
#define MBXRDCLRC21  (COREMBXRDCLR + 0x24)
#define MBXRDCLRC22  (COREMBXRDCLR + 0x28)
#define MBXRDCLRC23  (COREMBXRDCLR + 0x2C)

#define MBXRDCLRC30  (COREMBXRDCLR + 0x30)
#define MBXRDCLRC31  (COREMBXRDCLR + 0x34)
#define MBXRDCLRC32  (COREMBXRDCLR + 0x38)
#define MBXRDCLRC33  (COREMBXRDCLR + 0x3C)

void start_other_core ( unsigned int core, void (*function)(void) );
void other_core ( void );
void getMailboxes( unsigned int );

#endif
