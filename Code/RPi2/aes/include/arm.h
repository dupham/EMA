/**
* @Author: kevin, ronan
* @Date:   31-08-2016
* @Email:  sebanjila.bukasa@inria.fr, ronan.lashermes@inria.fr
* @Last modified by:   kevin
* @Last modified time: 06-10-2016
* @License: GPL
*/

#ifndef ARM_H
#define ARM_H

#define GPPUD           0x3F200094
#define GPPUDCLK0       0x3F200098
#define AUX_ENABLES     0x3F215004
#define AUX_MU_IO_REG   0x3F215040
#define AUX_MU_IER_REG  0x3F215044
#define AUX_MU_IIR_REG  0x3F215048
#define AUX_MU_LCR_REG  0x3F21504C
#define AUX_MU_MCR_REG  0x3F215050
#define AUX_MU_LSR_REG  0x3F215054
#define AUX_MU_MSR_REG  0x3F215058
#define AUX_MU_SCRATCH  0x3F21505C
#define AUX_MU_CNTL_REG 0x3F215060
#define AUX_MU_STAT_REG 0x3F215064
#define AUX_MU_BAUD_REG 0x3F215068


#define GPIO_BASE 0x3F200000
#define TIMER_BASE 0x3F003000
#define TIMER_LO 0x3F003004
#define TIMER_HI 0x3F003008

extern void put32 (unsigned int destination, unsigned int value); // store value @ destination
extern unsigned int get32 (unsigned int adress); // obtain value @ adress
extern void dummy (void);//do nothing
extern unsigned int getMPIDR(); //obtain MPIDR value to see which core is in execution

extern void start_l1cache(void);//start caches
extern void start_vfp(void);//start Vector Floating Point
extern void core_wrapper( void ); //wrap functions to cores
#endif
