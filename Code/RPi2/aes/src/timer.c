/**
* @Author: kevin, ronan
* @Date:   02-09-2016
* @Email:  sebanjila.bukasa@inria.fr, ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 02-09-2016
* @License: GPL
*/

#include "timer.h"
#include "arm.h"
#include "mem.h"

void timer_init(void) {
  put32(TIMER_BASE,0x00000000);
  put32(TIMER_BASE,0x00000200);
}

unsigned long get_timer_tick(void) {
  unsigned long hi = (unsigned long)get32(TIMER_HI);
  unsigned long shifted = hi << 32;
  return shifted | ((unsigned long)get32(TIMER_LO));
}

void wait_us(unsigned long us) {
  unsigned long tick_start = get_timer_tick();
  while(get_timer_tick() - tick_start < us) {
    dummy();
  }
}
