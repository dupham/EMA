<!--
@Author: ronan
@Date:   31-08-2016
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 09-09-2016
@License: GPL
-->



# Generalities

In this chapter, we recall some generic stuff about the RPi 2.
