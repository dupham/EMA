<!--
@Author: ronan
@Date:   31-08-2016
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 09-09-2016
@License: GPL
-->



Documentation is generated from Markdown files with **mdbook** (see project's GitHub page for installation).
Pretty doc can be accessed from "the book/index.html" file with your favorite browser.

```
mdbook build
```
