/**
* @Author: kevin
* @Date:   21-10-2016
* @Email:  sebanjila.bukasa@inria.fr
* @Last modified by:   kevin
* @Last modified time: 09-11-2016
* @License: GPL
*/

#ifndef PIN_H
#define PIN_H


#define TAILLE_PIN   4
#define DIFF_FAUX   0x5555
#define DIFF_VRAI   0xAAAA
#define STATUS_FAUX 0x5555
#define STATUS_VRAI 0xAAAA
#define FAKE_FAUX   0x5555
#define FAKE_VRAI   0xAAAA
#define FAUX        0x5555
#define VRAI        0xAAAA
#define MASTER    1
#define CANDIDATE 2
#define LENGTH    3
#define FAST      4

void verify_pin ( void );
void compare_pin ( void );
void set_pin ( unsigned int * value, unsigned int type );
void send_result ( unsigned int fake );

#endif
