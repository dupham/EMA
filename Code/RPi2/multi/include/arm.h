/**
* @Author: kevin, ronan
* @Date:   31-08-2016
* @Email:  sebanjila.bukasa@inria.fr, ronan.lashermes@inria.fr
* @Last modified by:   kevin
* @Last modified time: 02-12-2016
* @License: GPL
*/

#ifndef ARM_H
#define ARM_H

#define GPPUD           0x3F200094
#define GPPUDCLK0       0x3F200098
#define AUX_ENABLES     0x3F215004
#define AUX_MU_IO_REG   0x3F215040
#define AUX_MU_IER_REG  0x3F215044
#define AUX_MU_IIR_REG  0x3F215048
#define AUX_MU_LCR_REG  0x3F21504C
#define AUX_MU_MCR_REG  0x3F215050
#define AUX_MU_LSR_REG  0x3F215054
#define AUX_MU_MSR_REG  0x3F215058
#define AUX_MU_SCRATCH  0x3F21505C
#define AUX_MU_CNTL_REG 0x3F215060
#define AUX_MU_STAT_REG 0x3F215064
#define AUX_MU_BAUD_REG 0x3F215068


#define GPIO_BASE 0x3F200000
#define TIMER_BASE 0x3F003000
#define TIMER_LO 0x3F003004
#define TIMER_HI 0x3F003008

extern void put32 (unsigned int destination, unsigned int value); // store value @ destination
extern unsigned int get32 (unsigned int adress); // obtain value @ adress
extern void dummy (void);//do nothing
extern unsigned int getMPIDR(); //obtain MPIDR value to see which core is in execution
extern unsigned int getACTLR( void ); //get ACTLR of the current core
extern unsigned int getCPSR( void ); //get CPSR
extern unsigned int getPC( void ); //get PC
extern unsigned int* getSP(); //get SP
extern unsigned int getSCR( void ); //get SCR
extern unsigned int getISR( void ); //get ISR
extern unsigned int getMVBAR( void ); //get MVBAR
extern unsigned int getNSACR( void ); //get NSACR
extern unsigned int getSDER( void ); //get SDER
extern unsigned int getVBAR( void ); //get VBAR

extern void start_l1cache(void);//start caches
extern void start_vfp(void);//start Vector Floating Point
extern void core_wrapper( void ); //wrap to initiate cores
extern void core_func_wrapper( void ); //wrap to assign functions to cores
extern void ind_shutdwn( void ); //set an individual core into wait for interrupt mode (WFI)
extern void enb_smp( void ); //enable synchronous multiprocessing
extern void dis_smp( void ); //disable synchronous multiprocessing
extern void wfi_func( unsigned int core ); //wait and execute functions
extern void other_core_loop( void);
extern void smc_call( void ); //get SP


/* Usage:
 op contains the operation wanted:
 0 : change security state
 1 : change processor current mode
 2 : copy monitor CPSR to C0 mailbox 3
 
 data contains the value
 to change security state :
   - 1 change secure state
   - 0 does nothing
 to change core mode send the requested value of the cpsr
*/
extern void pre_smc_call(unsigned int op, unsigned int data);

#endif
