/**
* Define all primitives relative to basic memory management here.
*
* @Author: ronan, kevin
* @Date:   31-08-2016
* @Email:  ronan.lashermes@inria.fr, sebanjila.bukasa@inria.fr
* @Last modified by:   ronan
* @Last modified time: 02-12-2016
* @License: GPL
*/
#ifndef MEM_H
#define MEM_H

//Write a value at an address
extern void put32(unsigned int address, unsigned int value);
//Read the value at this address
extern unsigned int get32(unsigned int address);

int strncmp(const char *s1, const char *s2, unsigned int n);
void *memcpy(void *dst, const void *src, unsigned int len);
#endif
