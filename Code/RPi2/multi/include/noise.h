/**
* @Author: kevin
* @Date:   30-09-2016
* @Email:  sebanjila.bukasa@inria.fr
* @Last modified by:   kevin
* @Last modified time: 06-10-2016
* @License: GPL
*/

#ifndef NOISE_H
#define NOISE_H

unsigned int xorshift ( void );
unsigned int pgcd ( unsigned int a, unsigned int b ) ;
unsigned int wait_cycles (unsigned int a);


#endif
