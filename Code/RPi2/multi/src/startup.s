; @Author: ronan
; @Date:   31-08-2016
; @Email:  ronan.lashermes@inria.fr
; @Last modified by:   ronan
; @Last modified time: 16-11-2016
; @License: GPL

//Inspired from  https://github.com/dwelch67/raspberrypi/blob/master/mmu/novectors.s

.section ".normalboot"
##.global __smc


.globl _start
_start:
    ldr pc, _reset_h
    ldr pc, _undefined_instruction_vector_h
    ldr pc, _software_interrupt_vector_h
    ldr pc, _prefetch_abort_vector_h
    ldr pc, _data_abort_vector_h
    ldr pc, _unused_handler_h
    ldr pc, _interrupt_vector_h
    ldr pc, _fast_interrupt_vector_h

    _reset_h:                           .word   _reset_
    _undefined_instruction_vector_h:    .word   undefined_instruction_vector
    _software_interrupt_vector_h:       .word   software_interrupt_vector
    _prefetch_abort_vector_h:           .word   prefetch_abort_vector
    _data_abort_vector_h:               .word   data_abort_vector
    _unused_handler_h:                  .word   _reset_
    _interrupt_vector_h:                .word   interrupt_vector
    _fast_interrupt_vector_h:           .word   fast_interrupt_vector

_reset_:
    ## Set the stack pointer, which progresses downwards through memory
    cpsid ifa   ;@ disable interrupts
    ldr  sp, =_stack_top

    ## Set VBAR to _start
    ldr r1, =_start
    mcr p15, 0, r1, c12, c0, 0

    ## //Quit hypervisor
    ## mrs r0, cpsr        @ Loading of the CPSR register.
    ## bic r0, r0, #0xFF   @ Resetting of the lower byte to 0.
    ## orr r0, r0, #0xD3   @ Initialization of the lower byte to 0xD3 --> SVC mode.
    ## msr spsr_cxsf, r0   @ Updating of the backup register.
    ## add r0, pc, #4      @ Computation of the PC register for the next instruction.
    ## msr ELR_hyp, r0     @ Backup of the PC register in ELR_hyp.
    ## eret                @ Loading of the PC register from ELR_hyp and the CPSR register from SPSR_hyp.

    ## ldr  sp, =_stack_top

    ## //copy both table (@_start and _@reset_h) from 0x8000 to 0x0000
    ## ldr     r0, =_start
    ## mov     r1, #0x0000
    ## ldmia   r0!,{r2, r3, r4, r5, r6, r7, r8, r9}
    ## stmia   r1!,{r2, r3, r4, r5, r6, r7, r8, r9}
    ## ldmia   r0!,{r2, r3, r4, r5, r6, r7, r8, r9}
    ## stmia   r1!,{r2, r3, r4, r5, r6, r7, r8, r9}

    // Run the kernel_init - should not return and will call kernel_main
    b       core_wrapper

.globl _inf_loop
_inf_loop:
    b       _inf_loop

undefined_instruction_vector:
    b       undefined_instuction_handler

software_interrupt_vector:
    b        pre_smc_call

prefetch_abort_vector:
    b       prefetch_abort_handler

data_abort_vector:
    b       data_abort_handler

interrupt_vector:
    b       interrupt_handler

fast_interrupt_vector:
    b       fast_interrupt_handler
