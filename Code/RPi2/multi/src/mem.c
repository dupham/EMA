/**
* @Author: kevin, ronan
* @Date:   31-08-2016
* @Email:  sebanjila.bukasa@inria.fr, ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 31-08-2016
* @License: GPL
*/



#include "mem.h"

//String compare
int strncmp(const char *s1, const char *s2, unsigned int n)
{
     if (n == 0)
         return (0);
     do {
         if (*s1 != *s2++)
             return (*(unsigned char *) s1 - *(unsigned char *) --s2);
         if (*s1++ == 0)
             break;
     } while (--n != 0);
     return (0);
}

/*
 * Copy @len bytes from @src to @dst
 */
void *memcpy(void *dst, const void *src, unsigned int len)
{
  const char *s = src;
  char *d = dst;
  while (len--)
    *d++ = *s++;
  return dst;
}
