; @Author: ronan, kevin
; @Date:   31-08-2016
; @Email:  ronan.lashermes@inria.fr
; @Last modified by:   ronan
; @Last modified time: 16-11-2016
; @License: GPL

.section ".text.multicore"

.globl put32
put32:
    str r1,[r0]
    bx lr

.globl get32
get32:
    ldr r0,[r0]
    bx lr

.globl getSP
getSP:
    str     sp, [sp]
    ldr r0,[sp]
    bx lr

.globl getPC
getPC:
    ldr r0,[pc]
    bx lr

.globl getCPSR
getCPSR:
    MRS   r0,CPSR
    bx lr

.globl getACTLR
getACTLR:
    MRC p15, 0, r0, c1, c0, 1
    bx  lr

 .globl start_l1cache
 start_l1cache:
     mov r0, #0
     mcr p15, 0, r0, c7, c1, 0 ;@ invalidate caches
     mcr p15, 0, r0, c8, c7, 0 ;@ invalidate tlb
     mrc p15, 0, r0, c1, c0, 0
     orr r0,r0,#0x1000 ;@ instruction
     orr r0,r0,#0x0800 ;@ branch prediction
     ## orr r0,r0,#0x0004 ;@ data
     mcr p15, 0, r0, c1, c0, 0
     bx lr

 .globl stop_l1cache
 stop_l1cache:
     mrc p15, 0, r0, c1, c0, 0
     bic r0,r0,#0x1000 ;@ instruction
     bic r0,r0,#0x0800 ;@ branch prediction
     ## bic r0,r0,#0x0004 ;@ data
     mcr p15, 0, r0, c1, c0, 0
     bx lr

.globl start_vfp
start_vfp:
    @ r1 = Access Control Register
    MRC p15, #0, r1, c1, c0, #2
     @enable full access for p10,11
    ORR r1, r1, #(0xf << 20)
     @access Control Register = r1
    MCR p15, #0, r1, c1, c0, #2
    MOV r1, #0
    @flush prefetch buffer because of FMXR below
    MCR p15, #0, r1, c7, c5, #4
    @and CP 10 & 11 were only just enabled
     @Enable VFP itself
    MOV r0,#0x40000000
     @FPEXC = r0
    FMXR FPEXC, r0

 .globl read_sctlr
 read_sctlr:
     mrc p15,0,r0,c1,c0,0
     bx lr

 .globl invalidate_tlbs
 invalidate_tlbs:
     mov r2,#0
     mcr p15,0,r2,c8,c7,0  ;@ invalidate tlb
     mcr p15,0,r2,c7,c10,4 ;@ Data Synchronuzation Barrier
     bx lr

.globl dummy
dummy:
     bx lr

.globl getMPIDR
getMPIDR:
    mrc p15,0,r0,c0,c0,5 ;@ MPIDR
    bx lr

.globl core_wrapper
core_wrapper:
    mrc p15,0,r0,c0,c0,5 ;@ MPIDR
    mov r1, #0xFF
    ands r1, r1, r0                  @ Core0
    bne wrapper
    ldr  sp, =_stack_top
    b kernel_preinit

wrapper:
    ldr r2, =_stack_top
    cmp r1,#1                        @ Core1
    beq core_one
    cmp r1,#2                        @ Core2
    beq core_two
    cmp r1,#3                        @ Core3
    beq core_three
    b .

core_one:
    mov r1, #0            ;@ set offset value
    movt r1, #0x4
    add r1, r1, r2        ;@ set stack offset
    mov sp, r1            ;@ set stack pointer
    mov r1,#0
    str r1,[sp]
    b wait_loop

core_two:
    mov r1, #0            ;@ set offset value
    movt r1, #0x8
    add r1, r1, r2        ;@ set stack offset
    mov sp, r1            ;@ set stack pointer
    mov r1,#0
    str r1,[sp]
    b wait_loop

core_three:
    mov r1, #0            ;@ set stack value
    movt r1, #0xC
    add r1, r1, r2        ;@ set stack offset
    mov sp, r1            ;@ set stack pointer
    mov r1,#0
    str r1,[sp]
    b wait_loop

wait_loop:
    mrc p15,0,r0,c0,c0,5 ;@ MPIDR
    bl wfi_func
    b wait_loop

.globl core_func_wrapper
core_func_wrapper:
    mrc p15,0,r0,c0,c0,5 ;@ MPIDR
    mov r1, #0xFF
    ands r1, r1, r0                  @ Core0
    bne func_wrapper
    b kernel_main

func_wrapper:
    ldr r2, =_stack_top
    cmp r1,#1                        @ Core1
    beq func_core_one
    cmp r1,#2                        @ Core2
    beq func_core_two
    cmp r1,#3                        @ Core3
    beq func_core_three
    b .

func_core_one:
core_one_loop:
    movw r0, #0xC6C0
    movt r0, #0x2D
    bl wait_cycles
    bl other_core
    b wait_loop

func_core_two:
    movw r0, #0x4240
    movt r0, #0xF
    bl wait_cycles
core_two_loop:
    movw r0, #0xC6C0
    movt r0, #0x2D
    bl wait_cycles
    bl other_core
    b wait_loop

func_core_three:
    movw r0, #0x8480
    movt r0, #0x1E
    bl wait_cycles
core_three_loop:
    movw r0, #0xC6C0
    movt r0, #0x2D
    bl wait_cycles
    bl other_core
    b wait_loop

.globl other_core_loop
other_core_loop:
    bl wfi_func
    bl other_core
    b other_core_loop

.globl ind_shutdwn
ind_shutdwn:
   ;@ mov r1, #65503
    mrc p15, 0, r0, c1, c0, 0
    orr r0,r0,#0x0004 ;@ data
    mcr p15, 0, r0, c1, c0, 0
    clrex
    mrc p15,0,r0,c1,c0,1
    ands r0,r0, #0x1f ;@ #0xffdf disable SMP bit on ACTLR
    mcr p15,0,r0,c1,c0,1
    isb
    dsb
    wfi ;@ wait for interrupt

.globl dis_smp
dis_smp:
    ;@ mov r1, #65503
    mrc p15,0,r0,c1,c0,1
    ands r0,r0,#0x1f ;@ #0xffdf disable SMP bit on ACTLR
    mcr p15,0,r0,c1,c0,1
    bx lr

.globl enb_smp
enb_smp:
    ;@ mov r1, #65503
    mrc p15,0,r0,c1,c0,1
    orr r0,r0,#0x1f ;@ #0xffdf enable SMP bit on ACTLR
    mcr p15,0,r0,c1,c0,1
    bx lr

.globl pre_smc_call
pre_smc_call: ;@ invalidate caches
    mov r12,#0
    mcr p15, 0, r12, c7, c10, 1
    dsb
    mov r12, #0
    mcr p15, 0, r12, c7, c5, 0
    mov r12, #0
    mcr p15, 0, r12, c7, c5, 6
    dsb
    isb
    smc #0
    mov pc, lr

.globl smc_call
smc_call:
##    bl display_cpsr
    mrc p15, 0, r1, c1, c1, 0 ;@ read Secure Configuration Register
    bic r1, r1, #1
    mcr p15, 0, r1, c1, c1, 0
##    bl display_cpsr
    movs    pc, lr

;@ Security extensions accesses registers

;@ Secure Configuration Register
.globl getSCR
getSCR:
##    smc #0
    mrc p15, 0, r0, c1, c1, 0 ;@ read SCR
    bx lr

;@ Interrupt Status Register
.globl getISR
getISR:
    mrc p15, 0, r0, c12, c1, 0 ;@ read ISR
    bx lr


;@ Monitor Vector Base Adress Register
.globl getMVBAR
getMVBAR:
    mrc p15, 0, r0, c12, c0, 1 ;@ read MVBAR
    bx lr


;@ Non-Secure Access Control Register
.globl getNSACR
getNSACR:
    mrc p15, 0, r0, c1, c1, 2 ;@ read NSACR
    bx lr


;@ Secure Debug Enable Register
.globl getSDER
getSDER:
    mrc p15, 0, r0, c1, c1, 1 ;@ read SDER
    bx lr


;@ Vector Base Adress Register
.globl getVBAR
getVBAR:
    mrc p15, 0, r0, c12, c0, 0 ;@ read VBAR
    bx lr

.globl enable_user_mode
enable_user_mode:
    mrs r0, cpsr        @ Loading of the CPSR register.
    bic r0, r0, #0xFF   @ Resetting of the lower byte to 0.
    orr r0, r0, #0xD0   @ Initialization of the lower byte to 0xD3 --> SVC mode.
    msr spsr_cxsf, r0   @ Updating of the backup register.
    add r0, pc, #4      @ Computation of the PC register for the next instruction.
    msr ELR_hyp, r0     @ Backup of the PC register in ELR_hyp.
    eret                @ Loading of the PC register from ELR_hyp and the CPS


## .global monitor
## .align 5
## monitor:
## 	@ Monitor
## 	NOP     @ Reset      - not used by Monitor
## 	NOP     @ Undef      - not used by Monitor
## 	B       smc_call
## 	NOP     @ Prefetch   - can by used by Monitor
## 	NOP     @ Data abort - can by used by Monitor
## 	NOP     @ RESERVED
## 	NOP     @ IRQ        - can by used by Monitor
## 	NOP     @ FIQ        - can by used by Monitor

## @ ------------------------------------------------------------
## @ Monitor Initialization
## @
## @ This is called the first time the Secure world wishes to
## @ move to the Normal world.
## @ ------------------------------------------------------------

## .global monitorInit
## monitorInit:
## 	@ Install Secure Monitor
## 	@ -----------------------
## 	LDR r1, =ns_image                    /* R1 is used !!!!*/
## 	STR r0, [r1]
## 	LDR r0, =monitor                 @ Get address of Monitors vector table
## 	MCR p15, 0, r0, c12, c0, 1       @ Write Monitor Vector Base Address Register

## 	@ Save Secure state
## 	@ ------------------
## 	LDR     r0, =S_STACK_LIMIT          @ Get address of Secure state stack
## 	STMFD   r0!, {r4-r12}               @ Save general purpose registers
## 	@ ADD support for SPs
## 	MRS     r1, cpsr                    @ Also get a copy of the CPSR
## 	STMFD   r0!, {r1, lr}               @ Save CPSR and LR

## 	@ Switch to Monitor mode
## 	@ -----------------------
## 	CPS     #0x16                   @ Move to Monitor mode after saving Secure state

## 	@ Save Secure state stack pointer
## 	@ --------------------------------
## 	LDR     r1, =S_STACK_SP              @ Get address of global
## 	STR     r0, [r1]                     @ Save pointer


## 	@ Set up initial NS state stack pointer
## 	@ --------------------------------------
## 	LDR     r0, =_stack_bottom             @ Get address of global
## 	LDR     r1, =_stack_top          @ Get top of Normal state stack (assuming FD model)
## 	STR     r1, [r0]                     @ Save pointer


## 	@ Set up exception return information
## 	@ ------------------------------------
## 	@IMPORT  ns_image
	 
## 	LDR     lr, ns_image              @ ns_image
## 	MSR     spsr_cxsf, #0x13         @ Set SPSR to be SVC mode

## 	@ Switch to Normal world
## 	@ -----------------------
## 	MRC     p15, 0, r4, c1, c1, 0        @ Read Secure Configuration Register data
## 	ORR     r4, #0x1                  @ Set NS bit
## 	MCR     p15, 0, r4, c1, c1, 0        @ Write Secure Configuration Register data
	 
	 
## 	@ Clear general purpose registers
## 	@ --------------------------------
## 	MOV     r0,  #0
## 	MOV     r1,  #0
## 	MOV     r2,  #0
## 	MOV     r3,  #0
## 	MOV     r4,  #0
## 	MOV     r5,  #0
## 	MOV     r6,  #0
## 	MOV     r7,  #0
## 	MOV     r8,  #0
## 	MOV     r9,  #0
## 	MOV     r10, #0
## 	MOV     r11, #0
## 	MOV     r12, #0
 
## 	MOVS    pc, lr

## @ ------------------------------------------------------------
## @ Space reserved for stacks
## @ ------------------------------------------------------------
## NS_STACK_BASE:
## 	.word     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
## NS_STACK_LIMIT:

## S_STACK_BASE:
## 	.word     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
## S_STACK_LIMIT:

## NS_STACK_SP:
## 	.word     0
	
## S_STACK_SP:
## 	.word     0

## ns_image :
## 	.word     0


