/**
* @Author: kevin
* @Date:   21-10-2016
* @Email:  sebanjila.bukasa@inria.fr
* @Last modified by:   kevin
* @Last modified time: 09-11-2016
* @License: GPL
*/

#include "pin.h"
#include "gpio.h"
#include "uart.h"

unsigned int true_pin[TAILLE_PIN];
unsigned int candidate_pin[TAILLE_PIN];
unsigned int status = STATUS_FAUX ;
unsigned int pin_length = 1, c_set = 0, m_set = 0;

void verify_pin ( void ){
  if ( c_set == VRAI && m_set == VRAI){
    //toggle_trigger();
    compare_pin();
    //toggle_trigger();
  } else {
    uart_print_msg("candidate and/or master PIN is missing");
  }
}

void compare_pin ( void ){
  unsigned int fake = FAKE_FAUX, diff = DIFF_FAUX ;
  unsigned int i;
  
  /* for (i = 0; i < TAILLE_PIN; i++){ */
  /*   if ( true_pin[i] == candidate_pin[i] ){ */
  /*     diff = DIFF_FAUX ; */
  /*   } else { */
  /*     diff = DIFF_VRAI ; */
  /*   }     */
  /*   if ( diff == DIFF_VRAI ) { */
  /*     fake = FAKE_VRAI ; */
  /*     status = STATUS_FAUX ; */
  /*   } else if ( diff == DIFF_FAUX ) { */
  /*     status = STATUS_VRAI ; */
  /*     // fake = FAKE_FAUX ; */
  /*   } */
  fast_trig_up();
  for (i = 0; i < pin_length; i++){
    if (true_pin[i] == candidate_pin[i]){
      fake = VRAI ;
    }
    else {
      diff = VRAI ;
    }
  }
  fast_trig_down();
  if ( diff == VRAI ){
    fake = VRAI ; 
  }
  else {
    status = VRAI ; 
  }
  //  c_set = FAUX;
  //  m_set = FAUX;
  send_result(fake);
}

void send_result ( unsigned int fake ){
  if ( status == VRAI ){
    uart_print_msg("TRUE");
    status = FAUX ;
  } else if ( fake == VRAI ) { 
    uart_print_msg("FALSE");
  } else {
    uart_print_msg("Pin corrupted");
  }
}

void set_pin ( unsigned int * value, unsigned int type ){
  int count ;
  if (type == MASTER){
    for (count = 0; count < pin_length; count ++){
      true_pin[count] = value[count];
    }
    m_set = VRAI ;
  } else if (type == CANDIDATE){
    for (count = 0; count < pin_length; count ++){
      candidate_pin[count] = value[count];
    }
    c_set = VRAI ;
  } else if (type == LENGTH){
    pin_length = value[0];
  } else if (type == FAST){
    true_pin[0] = 1 ;
    candidate_pin[0] = value[0] ;
    m_set = VRAI ;
    c_set = VRAI ;
  }
}
