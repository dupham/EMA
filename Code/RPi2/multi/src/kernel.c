/**
* @Author: ronan, kevin
* @Date:   31-08-2016
* @Email:  ronan.lashermes@inria.fr, sebanjila.bukasa@inria.fr
* @Last modified by:   ronan
* @Last modified time: 02-12-2016
* @License: GPL
*/


#include "types.h"
#include "kernel.h"
#include "arm.h"
#include "gpio.h"
#include "uart.h"
#include "timer.h"
#include "jtag.h"
#include "aes.h"
#include "noise.h"
#include "core.h"
#include "pin.h"
#include "wdog.h"
#include <stdlib.h>
#include <stdio.h>

int kernel_preinit ( unsigned int r0, unsigned int r1, unsigned int r2 )
{
  initiate_jtag();
  timer_init();
  uart_init();
  gpio_init();
  
  //Erase BSS
  unsigned int* bss  = &__bss_start__;
  unsigned int*  bss_end = &__bss_end__ ;

  //Initialize bss section to 0
  while(bss < bss_end) {
    *bss++ = 0;
  }

  //start caches
  start_l1cache();
  start_vfp();

  // start other cores (initialize stack vectors)
  start_other_core (MBXSETC13, core_wrapper);
  start_other_core (MBXSETC23, core_wrapper);
  start_other_core (MBXSETC33, core_wrapper);

  //call main after preinit
  kernel_main(r0, r1, r2);

  return 0;
}

/**************************************************/
/*                  Main                          */
/**************************************************/
int kernel_main ( unsigned int r0, unsigned int r1, unsigned int r2 )
{
  unsigned int ra;
  unsigned int rb=0;
  unsigned int menu=SUMMARY, command;
  unsigned char buffer[16];
  unsigned char key[16], out[16], in[16];
  unsigned int count_byte, pin[10];
  unsigned int input_status=0, key_status=0, cipher_status=0, pin_length=1, fast_mode, ns=1;
  
  unsigned char test_plain[DATA_SIZE] = {0x32, 0x43, 0xf6, 0xa8, 0x88, 0x5a, 0x30, 0x8d, 0x31, 0x31, 0x98, 0xa2, 0xe0, 0x37, 0x07, 0x34};
  unsigned char test_key[DATA_SIZE] = {0x2b, 0x7e, 0X15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c};
  unsigned char test_cipher[DATA_SIZE] = {0x39, 0x25, 0x84, 0x1d, 0x02, 0xdc, 0x09, 0xfb, 0xdc, 0x11, 0x85, 0x97, 0x19, 0x6a, 0x0b, 0x32};


  //  uart_init();
  //gpio_init();
  fast_mode = DISABLE;

  //hexstrings(GETSCTLR());

  // MAIN MENU
  while(1){
    if(menu == SUMMARY){
      // send_msg(3);
      uart_print_msg("Select command");
      uart_print_msg("a for AES mode");
      uart_print_msg("e for LED mode");
      uart_print_msg("p for PIN mode");
      uart_print_msg("t for test trigger");
      uart_print_msg("m to activate multicore");
      uart_print_msg("r to change mode");
      menu = uart_recv();
      switch(menu){
      case 'a':
      case 'A':
	menu = AES_MODE;
	break;
      case 'e':
      case 'E':
	menu = LED_MODE;
	break;
      case 'p':
      case 'P':
	menu = PIN_MODE;
	break;
      case 't':
      case 'T':
	menu = TT_MODE;
	break;
      case 'm':
      case 'M':
	menu = MUL_MODE;
	break;
      case 'r':
      case 'R':
	menu = REG_MODE;
      default:
	break;

      }
    }

    // AES_MODE

    else if(menu == AES_MODE){
      command = uart_recv();
      //uart_print_msg((char*)&command);
      switch(command){
      case 't'://test connection
	printf("Comms OK\n");
	unsigned int i;
	for(i = 0; i < DATA_SIZE; i++) {
	  in[i] = test_plain[i];
	  key[i] = test_key[i];
	}
	AESEncrypt(out,in,key);
	bool test = true;
	for(i = 0; i < DATA_SIZE; i++) {
	  if(out[i]!=test_cipher[i]) {
	    test = false;
	  }
	}
	if(test) {
	printf("AES test OK\n");
	} else {
	  printf("AES test FAIL:\n");
	  printf("Plaintext: ");
	  uart_print_hexvector(in, DATA_SIZE);
	  printf("\nKey: ");
	  uart_print_hexvector(key, DATA_SIZE);
	  printf("\nCiphertext: ");
	  uart_print_hexvector(out, DATA_SIZE);
	  printf("\n");
	}
	break;
      case 'k':
	uart_read_vector(key, DATA_SIZE);
	break;
      case 'p':
	uart_read_vector(in, DATA_SIZE);
	break;
      case 'c':
	uart_send_vector(out, DATA_SIZE);
	break;
      case 'g':
	AESEncrypt(out, in, key);
      break;
      case 'f':
	uart_read_vector(in, DATA_SIZE);
	AESEncrypt(out, in, key);
	uart_send_vector(out, DATA_SIZE);
        break;
      case 's': // 73
      case 'S': // 53
	// statut actuel
	uart_print_msg("Key: ");
	uart_print_hexvector(key, DATA_SIZE);
	uart_print_msg("Plaintext: ");
	uart_print_hexvector(in, DATA_SIZE);
	break;
      case 'x':
      case 'X':
	// reset
	menu = SUMMARY;
	break;
      default:
	uart_print_msg("Unknown command");
      }
    }

    // PIN MODE

    else if(menu == PIN_MODE){
      // send_msg(2);
      uart_print_msg("Pin Mode:");
      uart_print_msg("s to set PIN length");
      uart_print_msg("c for Candidate");
      uart_print_msg("m for Master");
      uart_print_msg("f for Fast Mode");
      command=uart_recv();
      switch(command){
      case 's':
      case 'S':
    	uart_print_msg("Set pin length");
    	pin[0] = uart_recv();
    	if ( pin[0] < MAX_PIN ){
	  pin_length = pin[0];
	  set_pin(pin,LENGTH);
	}
	else {
    	  uart_print_msg("size must be between 1 and 4");
    	}
    	break;
      case 'c':
      case 'C':
    	uart_print_msg("Set candidate pin");
    	for ( count_byte = 0 ; count_byte < pin_length ; count_byte++ ){
    	  pin[count_byte] = uart_recv();
    	}
	set_pin(pin,CANDIDATE);
    	break;
      case 'm':
      case 'M':
    	uart_print_msg("Set master pin");
    	for ( count_byte = 0 ; count_byte < pin_length ; count_byte++ ){
    	  pin[count_byte] = uart_recv();
    	}
	set_pin(pin,MASTER);
    	break;
      case 'v':
      case 'V':
	verify_pin();
	break;
      case 'f':
      case 'F':
    	uart_print_msg("Fastmode -- Master PIN = 1");
    	pin[0] = uart_recv();
    	set_pin(pin,FAST);
	verify_pin();
    	break;
      case 'x':
      case 'X':
	uart_print_msg("Back to Main menu");
    	menu = SUMMARY;
    	break;
      default:
    	uart_print_msg("Unknown command");
      }
    }

    // LED MODE

    else if(menu == LED_MODE) {
      rb=uart_recv();
      if((rb == 'A')||(rb == 'a')){
	uart_send(rb);
       	toggle_LED();
      }
      if((rb == 'B')||(rb == 'b')){
	uart_send(rb);
	toggle_LED();
	ra++;
      }
      if((rb == 'R')||(rb == 'r')){
	menu = SUMMARY;
      }
    }

    // MUL MODE

    else if(menu == MUL_MODE) {
      uart_print_msg("Start core");
      uart_print_msg("Core 1: a");
      uart_print_msg("Core 2: b");
      uart_print_msg("Core 3: c");
      uart_print_msg("All   : d");
      uart_print_msg("Status: s");
      uart_print_msg("Test  : t");
      uart_print_msg("Other : o");
      rb=uart_recv();
      if((rb == 'A')||(rb == 'a')){
	uart_print_msg("Core 1 started");
	start_other_core (MBXSETC13, other_core_loop);
      }
      if((rb == 'B')||(rb == 'b')){
	uart_print_msg("Core 2 started");
	start_other_core (MBXSETC23, other_core_loop);
      }
      if((rb == 'C')||(rb == 'c')){
	uart_print_msg("Core 3 started");
	start_other_core (MBXSETC33, other_core_loop);
      }
      if((rb == 'D')||(rb == 'd')){
	uart_print_msg("All cores started");
	start_other_core (MBXSETC13, core_func_wrapper);
	start_other_core (MBXSETC23, core_func_wrapper);
	start_other_core (MBXSETC33, core_func_wrapper);
      }
      if((rb == 'G')||(rb == 'g')){
	uart_print_msg("Clear core 0 mbx");
	put32((COREMBXRDCLR + 12),0xFFFFFFFF);
	put32((COREMBXRDCLR + 8),0xFFFFFFFF);
	put32((COREMBXRDCLR + 4),0xFFFFFFFF);
	put32((COREMBXRDCLR),0xFFFFFFFF);
      }
      if((rb == 'S')||(rb == 's')){
	int * mailboxes;
	uart_print_msg("Core status");
	uart_print_msg("Core 0");
	uart_print_32bvector(getMailboxes(CORE0),4);
	uart_send(0x0D);
	uart_send(0x0A);
	uart_print_msg("Core 1");
	uart_print_32bvector(getMailboxes(CORE1),4);
	uart_send(0x0D);
	uart_send(0x0A);
	uart_print_msg("Core 2");
	uart_print_32bvector(getMailboxes(CORE2),4);
	uart_send(0x0D);
	uart_send(0x0A);
	uart_print_msg("Core 3");
	uart_print_32bvector(getMailboxes(CORE3),4);
	uart_send(0x0D);
	uart_send(0x0A);
      }
      if((rb == 'T')||(rb == 't')){
	/* start_other_core(MBXSETC21, 0x21); */
	/* start_other_core(MBXSETC31, 0x31); */
	/* start_other_core(MBXSETC11, 0x11); */
      }
      if((rb == 'O')||(rb == 'o')){
	uart_print_msg("Mailboxes Interrupt control");
	uart_printhex(get32(0x40000050));
	uart_printhex(get32(0x40000054));
	uart_printhex(get32(0x40000058));
	uart_printhex(get32(0x4000005C));
      }
      if((rb == 'R')||(rb == 'r')){
	menu = SUMMARY;
      }

    }


    // TEST TRIGGER MODE

    else if(menu == TT_MODE) {
      rb=uart_recv();
      if((rb == 'M')||(rb == 'm')){
	uart_print_msg((char*)rb);
	toggle_LED();
	fast_trig_up();
	fast_trig_down();
      }
      /* if((rb == 'W')||(rb == 'w')){
	uart_send(rb);
	wdog_start(0xFFFF);
	rb = 0 ;
	ra=wdog_get_remaining();
	while(1){ // ra < OxC26F
	  if (rb%2 == 0){
	    toggle_led();
	    toggle_trigger();
	  }
	  rb++;
	  if (ra > 0x13){ // 3 ms
	    toggle_trigger();
	    trigger_off();
	    wdog_stop();
	    break;
	  }
	}
	}*/
      if((rb == 'S')||(rb == 's')){
	uart_print_msg("trigger ss cache");
	fast_trig_up();
	fast_trig_down();
	uart_print_msg("fin trigger ss cache");
      }
      if((rb == 'R')||(rb == 'r')){
	menu = SUMMARY;
      }
    }

    // REG MODE

    else if(menu == REG_MODE) {
      uart_print_msg("a: Send change secure state with 1");
      uart_print_msg("z: Change mode");
      uart_print_msg("d: Display current mode");
      uart_print_msg("e: Display secure registers");
      uart_print_msg("f: Display regular registers");
      uart_print_msg("r: Change to User mode");
      uart_print_msg("t: change to Supervisor");
      uart_print_msg("x: Return to main menu");
      rb=uart_recv();
      if((rb == 'A')||(rb == 'a')){
	uart_print_msg("Change NS bit");
	pre_smc_call(0,1);
	ns ^= 1;
	uart_printhex(ns);
	uart_send(0x0D);
	uart_send(0x0A);
      }
       if((rb == 'Z')||(rb == 'z')){
	 if(getVBAR() == 0x800){
	  uart_print_msg("From normal to secure mode");
	  pre_smc_call(0,1);
	  //	  pre_smc_call(2,0);
	  ns ^= 1;
	} else if(getVBAR() == 0x000){
	  uart_print_msg("From Secure mode to normal");
	  pre_smc_call(0,1);
	  //      pre_smc_call(2,0);
	  ns ^= 1;
	}
      }
      if((rb == 'D')||(rb == 'd')){
	if(getVBAR() == 0x800){
	  uart_print_msg("Normal mode");
	  uart_printhex(ns);
	  uart_send(0x0D);
	  uart_send(0x0A);
	} else if(getVBAR() == 0x000){
	  uart_print_msg("Secure mode");
	  uart_printhex(ns);
	  uart_send(0x0D);
	  uart_send(0x0A);
	} else uart_print_msg("Error on mode");
      }
      if((rb == 'E')||(rb == 'e')){
	if( ns == 1 ){
	  uart_print_msg("Not in secure state");
	} else {
	  uart_print_msg("SDER");
	  uart_printhex(getSDER());
	  uart_send(0x0D);
	  uart_send(0x0A);
	  uart_print_msg("SCR");
	  uart_printhex(getSCR());
	  uart_send(0x0D);
	  uart_send(0x0A);
	  uart_print_msg("VBAR");
	  uart_printhex(getVBAR());
	  uart_send(0x0D);
	  uart_send(0x0A);
	  uart_print_msg("MVBAR");
	  uart_printhex(getMVBAR());
	  uart_send(0x0D);
	  uart_send(0x0A);
	}
      }
      if((rb == 'F')||(rb == 'f')){
	uart_print_msg("CPSR");
	uart_printhex(getCPSR());
	uart_send(0x0D);
	uart_send(0x0A);
	uart_print_msg("ISR");
	uart_printhex(getISR());
	uart_send(0x0D);
	uart_send(0x0A);
	uart_print_msg("NSACR");
	uart_printhex(getNSACR());
	uart_send(0x0D);
	uart_send(0x0A);
	uart_print_msg("VBAR");
	uart_printhex(getVBAR());
	uart_send(0x0D);
	uart_send(0x0A);
	uart_print_msg("ACTLR");
	uart_printhex(getACTLR());
	uart_send(0x0D);
	uart_send(0x0A);
      }
      if((rb == 'R')||(rb == 'r')){
	uart_printhex(getCPSR());
	uart_send(0x0D);
	uart_send(0x0A);
	uart_print_msg("go to user mode");
	uart_print_msg("CPSR 1");
	ra=getCPSR();
	uart_printhex(ra);
	uart_send(0x0D);
	uart_send(0x0A);
	ra^=0x3;
	uart_print_msg("CPSR ^ USER mode");
	uart_printhex(ra);
	uart_send(0x0D);
	uart_send(0x0A);
	uart_print_msg("call smc");
	pre_smc_call(1,(getCPSR()^0x3));
	uart_printhex(getCPSR());
	uart_send(0x0D);
	uart_send(0x0A);
      }
      if((rb == 'T')||(rb == 't')){
	uart_printhex(getCPSR());
	uart_print_msg("go to supervisor");
	pre_smc_call(1,(getCPSR()^0x3));
	uart_printhex(getCPSR());
	uart_send(0x0D);
	uart_send(0x0A);
      }
      if((rb == 'X')||(rb == 'x')){
	menu = SUMMARY;
      }
    }

  }
  return(0);
}

void gpio_init() {
  //set LED as output
  set_pin_function(LED_PIN, FSEL_OUTPUT);

  //set LED2 as output
  set_pin_function(LED_PIN2, FSEL_OUTPUT);

  //set trig pin as output
  set_pin_function(TRIG_PIN, FSEL_OUTPUT);
}
