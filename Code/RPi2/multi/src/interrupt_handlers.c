/**
* @Author: ronan, kevin
* @Date:   31-08-2016
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   kevin
* @Last modified time: 08-12-2016
* @License: GPL
*/

#include "gpio.h"


void __attribute__((interrupt("UNDEF"))) undefined_instuction_handler(void)
{
  error_code_LED(5);
}

void __attribute__((interrupt("SWI"))) software_interrupt_handler(void)
{
  error_code_LED(6);
}

void __attribute__((interrupt("ABORT"))) prefetch_abort_handler(void)
{
  error_code_LED(7);
}

void __attribute__((interrupt("ABORT"))) data_abort_handler(void)
{
  error_code_LED(8);
}

void __attribute__((interrupt("IRQ"))) interrupt_handler(void)
{
  error_code_LED(9);
}

void __attribute__((interrupt("FIQ"))) fast_interrupt_handler(void)
{
  error_code_LED(10);
}


/* secure handlers */

void __attribute__((interrupt("UNDEF"))) secundefi_handler(void)
{
  error_code_LED(5);
}

void __attribute__((interrupt("ABORT"))) secpabort_handler(void)
{
  error_code_LED2(7);
}

void __attribute__((interrupt("ABORT"))) secdabort_handler(void)
{
  error_code_LED2(8);
}

void __attribute__((interrupt("IRQ"))) secirq_handler(void)
{
  error_code_LED2(9);
}

void __attribute__((interrupt("FIQ"))) secfiq_handler(void)
{
  error_code_LED2(10);
}
