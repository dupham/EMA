/**
* @Author: ronan
* @Date:   09-09-2016
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 09-09-2016
* @License: GPL
*/

// untested !!!!!!!!!!!!!!!!

#include "jtag.h"
#include "gpio.h"

int initiate_jtag()
{
    set_pin_function(4, FSEL_ALT5);
    set_pin_function(22, FSEL_ALT4);
    set_pin_function(24, FSEL_ALT4);
    set_pin_function(25, FSEL_ALT4);
    set_pin_function(27, FSEL_ALT4);

    //ARM_TRST      22 GPIO_GEN3 P1-15 IN  (22 ALT4)
    //ARM_TDO     5/24 GPIO_GEN5 P1-18 OUT (24 ALT4)
    //ARM_TCK    13/25 GPIO_GEN6 P1-22 OUT (25 ALT4)
    //ARM_TDI     4/26 GPIO_GCLK P1-7   IN ( 4 ALT5)
    //ARM_TMS    12/27 CAM_GPIO  S5-11 OUT (27 ALT4)
    //ARM_RTCK    6/23 not connected yet
    return(0);
}
