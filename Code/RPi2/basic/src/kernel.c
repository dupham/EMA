/**
* @Author: ronan, kevin
* @Date:   31-08-2016
* @Email:  ronan.lashermes@inria.fr, sebanjila.bukasa@inria.fr
* @Last modified by:   ronan
* @Last modified time: 03-10-2016
* @License: GPL
*/


#include "types.h"
#include "kernel.h"
#include "arm.h"
#include "gpio.h"
#include "uart.h"
#include "timer.h"
#include "jtag.h"
#include <stdlib.h>
#include <stdio.h>

/**************************************************/
/*               PROGRAM STATE                    */
/**************************************************/


/**************************************************/
/*               VALUE STATE                      */
/**************************************************/

/**************************************************/
/*               Init                      */
/**************************************************/

void gpio_init() {
  //set LED as output
  set_pin_function(LED_PIN, FSEL_OUTPUT);
}

int kernel_preinit ( unsigned int r0, unsigned int r1, unsigned int r2 )
{
  initiate_jtag();
  timer_init();
  uart_init();

  //Erase BSS
  unsigned int* bss  = &__bss_start__;
  unsigned int*  bss_end = &__bss_end__ ;

  //Initialize bss section to 0
  while(bss < bss_end) {
    *bss++ = 0;
  }

  //start caches
  start_l1cache();
  start_vfp();

  //call main after preinit
  kernel_main(r0, r1, r2);
}

/**************************************************/
/*                  Main                          */
/**************************************************/

int kernel_main ( unsigned int r0, unsigned int r1, unsigned int r2 )
{
  gpio_init();

  //test printf and malloc
  extern unsigned int _heap_start;

  printf("\nShall we play a game?>\n");


  unsigned int* counters;
  counters = malloc(10*sizeof(unsigned int));

  /* Failed to allocate memory! */
   if( counters == NULL )
       error_code_LED(2);

   int loop;
   for( loop=0; loop<10; loop++ )
       counters[loop] = 0;

  printf("heap start=0x%x\n", (unsigned int)&_heap_start);


  while(true) {
    turn_on_LED();
    wait_us(100000);
    turn_off_LED();
    wait_us(100000);
  }

  return 0;
}
