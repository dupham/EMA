/**
* @Author: ronan
* @Date:   31-08-2016
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 01-09-2016
* @License: GPL
*/


#ifndef KERNEL_H
#define KERNEL_H

extern unsigned int __bss_start__;
extern unsigned int __bss_end__;

#endif
