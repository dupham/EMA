/**
* @Author: ronan
* @Date:   01-09-2016
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 12-09-2016
* @License: GPL
*/



#ifndef TYPES_H
#define TYPES_H

#include <stdbool.h>

typedef unsigned char byte;

#endif
