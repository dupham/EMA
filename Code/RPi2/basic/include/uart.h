/**
* @Author: kevin, ronan
* @Date:   02-09-2016
* @Email:  sebanjila.bukasa@inria.fr, ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 12-09-2016
* @License: GPL
*/

#ifndef UART_H
#define UART_H

#include "types.h"

//get state register (if data available or waiting to be sent...)
unsigned int uart_get_reg ( void );
//read 1 byte of data
unsigned char uart_recv ( void );
//check if data available
bool uart_availble ( void );
//send 1 byte of data
void uart_send ( unsigned char c );
//flush input data
void uart_flush ( void );
//initialize uart
int uart_init ( void );
//print a string (prefer printf)
void uart_print(unsigned char* str);
//print a number in hex format
void uart_printhex ( unsigned int d );
//read [len] bytes of data
void uart_read_vector(unsigned char* vec, unsigned int len);
//write [len] bytes of data
void uart_send_vector(unsigned char* vec, unsigned int len);
//print a byte vector in hex format
void uart_print_hexvector(unsigned char* vec, unsigned int len);

#endif
