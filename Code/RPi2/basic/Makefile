# @Author: ronan
# @Date:   31-08-2016
# @Email:  ronan.lashermes@inria.fr
# @Last modified by:   ronan
# @Last modified time: 09-09-2016
# @License: GPL



# Specify the C compiler to use
CC=arm-none-eabi-gcc
# Specify the assembler to use
AS=arm-none-eabi-as
# Specity the linker to use
LD=arm-none-eabi-gcc
#The obj-copy to strip the elf of its metadata
OBJCPY=arm-none-eabi-objcopy
#Command to generate listing
OBJDUMP=arm-none-eabi-objdump

#dirs
SDIR =./src
ODIR =./obj
IDIR =./include
BDIR =./bir

#Targeting RPI2
GCC_TARGET = -mfpu=neon-vfpv4 -mfloat-abi=hard -march=armv7-a -mtune=cortex-a7

#C flags
CFLAGS = -O1
CFLAGS += $(GCC_TARGET)
CFLAGS += -I$(IDIR)

#Assembler flags
ASFLAGS += -mfpu=neon-vfpv4 -mfloat-abi=hard
# ASFLAGS += $(GCC_TARGET)

#--crate-type=lib

#Linker flags
LFLAGS = $(GCC_TARGET)
LFLAGS += -nostartfiles
# -nostdlib
LFLAGS += -ffreestanding
LFLAGS += -O1
#-ffreestanding -nostdlib --specs=nosys.specs

#:$(other)
vpath %.c $(SDIR)
vpath %.s $(SDIR)

#Dependencies
_DEPS =
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

#Object files to link
_OBJ = kernel.o interrupt_handlers.o startup.o arm.o gpio.o timer.o uart.o cstub.o jtag.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

#C compilation rule
$(ODIR)/%.o: %.c $(DEPS)
	@echo CC $<
	@$(CC) -c -o $@ $< $(CFLAGS)

#Asssembly compilation rule
$(ODIR)/%.o: %.s $(DEPS)
	@echo AS $<
	@$(AS) $(ASFLAGS) $< -o $@

all: kernel.elf

#Linker rule using specified linker script
kernel.elf: $(OBJ)
	@echo Linking...
	@$(LD) $(OBJ) $(LFLAGS) -T rpi2.ld -o $@
	@echo OK
	@$(OBJDUMP) -S kernel.elf > kernel.list
	@echo "Listing created"


.PHONY: obj-copy
obj-copy:
	@$(OBJCPY) kernel.elf -O binary ./bin/kernel.img
	@echo kernel.img created

SD_CARD = /media/ronan/RPi

.PHONY: burn
burn:
	@$(OBJCPY) kernel.elf -O binary ./bin/kernel.img
	@echo kernel.img created.
	@sudo cp ./bin/kernel.img $(SD_CARD)
	@echo BURNT!
	@sudo umount $(SD_CARD)
	@echo umounted

#Cleaning
.PHONY: clean
clean:
	rm -f $(ODIR)/*.o *~ $(INCDIR)/*~ $(SDIR)/*~
